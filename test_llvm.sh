#!/bin/bash -l
module purge
module load esslurm
module load cdt/19.11
module load PrgEnv-cray
# module switch cce cce/9.1.0
module switch cce cce/9.1.3
module load craype-x86-skylake
module unload cray-libsci
module load cudatoolkit craype-accel-nvidia70
module load cuda
module list
set -x
export OMP_NUM_THREADS=5

make -f Makefile_cray_llvm clean
make -f Makefile_cray_llvm spmm_csr

# CSR
srun -n 1 -c 10 --cpu_bind=cores ./SpMM_mini_app_openmp.x 12 ../../Matrices/twitter7_top_left_CSR.dat
# srun -n 1 -c 40 --cpu_bind=cores ./SpMM_mini_app_openmp.x 12 ../../Matrices/twitter7_top_right_CSR.dat

# COO
# srun -n 1 -c 10 --cpu_bind=cores ./SpMM_mini_app_coo.x 8 ../../Matrices/twitter7_top_left_COO.dat
# srun -n 1 -c 40 --cpu_bind=cores ./SpMM_mini_app_openmp.x 12 ../../Matrices/twitter7_top_right_CSR.dat
