#include <iostream>
#include <ctime>
#include <sys/time.h>

using namespace std;

// link to original post: https://stackoverflow.com/questions/21428378/reduce-matrix-columns-with-cuda

// nvcc cuda_reduction.cu -o cuda_reduction.x -arch=sm_70
// srun -n 1 -c 1 ./cuda_reduction.x.cux 
// 4 times wraps

template<typename T>
__global__ 
void kernelSum(const T* __restrict__ input, 
        const size_t lda, // pitch of input in words of sizeof(T)
        T* __restrict__ per_block_results, 
                const size_t n)
{
    extern __shared__ T sdata[];

    T x = 0.0;
    const T * p = &input[blockIdx.x * lda];
    
    // Accumulate per thread partial sum ==> 256 numbers
    for(int i=threadIdx.x; i < n; i += blockDim.x) {
        x += p[i];
    }

    // load thread partial sum into shared memory
    sdata[threadIdx.x] = x;
    __syncthreads();

   

    for(int offset = blockDim.x / 2; offset > 0; offset >>= 1) {
        if(threadIdx.x < offset) {
            sdata[threadIdx.x] += sdata[threadIdx.x + offset];
        }
        __syncthreads();
    }

    // thread 0 writes the final result
    if(threadIdx.x == 0) {
        per_block_results[blockIdx.x] = sdata[0];
    }
}

double get_seconds() 
{
	struct timeval now;
	gettimeofday(&now, NULL);
	const double seconds = (double) now.tv_sec;
	const double usec    = (double) now.tv_usec;
	return seconds + (usec * 1.0e-6);
}


int main(void)
{
    // mawi dim
    const int m = 128568730, n = 8;

    // twitter dim
    // const int m = 41652230, n = 16;

    double * a = new double[m * n];

    for(int i = 0 ; i < (m * n); i++) 
    { 
        a[i] = 0.00075 ; //(double)(i%10); 
    }

    double *a_;
    size_t size_a = m * n * sizeof(double);
    cudaMalloc((void **)&a_, size_a);
    cudaMemcpy(a_, a, size_a, cudaMemcpyHostToDevice);

    double *b_;
    size_t size_b = n * sizeof(double);
    cudaMalloc((void **)&b_, size_b);

    // unsigned int *blkDimX_;
    // cudaMalloc((void **)&blkDimX_, sizeof(unsigned int));

    // select number of warps per block according to size of the
    // colum and launch one block per column. Probably makes sense
    // to have at least 4:1 column size to block size
    dim3 blocksize(256); 
    dim3 gridsize(n);
    size_t shmsize = sizeof(double) * (size_t)blocksize.x;

    cout << "blocksize.x: " << (size_t)blocksize.x << " blocksize.y: " << (size_t)blocksize.y << " blocksize.z: " << (size_t)blocksize.z << endl;
    cout << "gridsize.x: " << (size_t)gridsize.x << " gridsize.y: " << (size_t)gridsize.y << " gridsize.z: " << (size_t)gridsize.z << endl;

    double tstart = get_seconds();
    
    kernelSum<double><<<gridsize, blocksize, shmsize>>>(a_, m, b_, m);
    
    cudaDeviceSynchronize();
    double tend = get_seconds();

    std::cout << "kernelSum time: " << tend - tstart << " sec." << endl;

    double * b = new double[n];
    cudaMemcpy(b, b_, size_b, cudaMemcpyDeviceToHost);

    for(int i=0; i<n; i++) {
       std::cout << i << " " << b[i] << std::endl;
    }

    // int *blkX_ = new int[1];
    // cudaMemcpy(blkX_, blkDimX_, sizeof(int), cudaMemcpyDeviceToHost);
    // cudaDeviceSynchronize();

    // cout << "blkDimX_: " << blkX_ << endl;

    cudaDeviceReset();

    return 0;
} 