#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#include "omp.h"

#include <cuda_runtime.h>
#include "cublas_v2.h"

#define DGEMM_RESTRICT __restrict__

/*
How to compile: nvcc -Xcompiler \-fopenmp -lgomp cublasDgemm_xy.cu -o cublasDgemm_xy_nvcc.x -lcudart -lcublas -lnvToolsExt
How to run: srun -n 1 -c 10 --cpu_bind=cores ./cublasDgemm_xy_nvcc.x 100000000 8 8 262144
*/

double get_seconds() 
{
	struct timeval now;
	gettimeofday(&now, NULL);

	const double seconds = (double) now.tv_sec;
	const double usec    = (double) now.tv_usec;

	return seconds + (usec * 1.0e-6);
}


void set_zero_host_matrix(double *matrixC, int row, int col)
{
	int i;
	#pragma omp parallel for
	for(i = 0; i < row * col; i++) 
	{
		matrixC[i] = 0.0;
	}
}

void sanity_check(double *matrixC, int row, int col, int val)
{
	int i, j;
	double allowed_margin = 1.0e-8;

	for(i = 0 ; i < row ; i++)
	{
		for(j = 0 ; j < col ; j++)
		{
			if((matrixC[i * col + j] - val) > allowed_margin)
			{
				printf(" -> Solution check FAILED.\n");
				return;
			}
		}
	}
	printf(" -> Solution check PASSED successfully.\n\n");
}

void copy_to_device(double *devPtr, double *hostPtr, int row, int col)
{
	cudaError_t cuberror;
	cuberror = cudaMemcpy((void *)devPtr, hostPtr, row * col * sizeof(double), cudaMemcpyHostToDevice);
  	cudaDeviceSynchronize();
	if( cuberror != cudaSuccess ){ printf("cudaMemcpyFailure on devPtrC\n"); return; }
}

int main(int argc, char* argv[]) 
{
	int N = 30, M, P;
	int repeats = 1;

	double alpha = 1.0;
	double beta  = 0.0;
	double gpu_memory, tstart, tend;

	int blksz;

	N = atoi(argv[1]);
	M = atoi(argv[2]);
	P = atoi(argv[3]);
		

	int block_width = atoi(argv[4]);
  	
	printf("                  N =  %d\n", N);
	printf("                  M =  %d\n", M);
	printf("                  P =  %d\n", P);
    printf("              Alpha =  %0.3lf\n", alpha);
    printf("              Beta  =  %0.3lf\n", beta);
	printf("            Repeat  =  %d\n", repeats);
	printf("         Tile size  =  %d\n", block_width);

	double* DGEMM_RESTRICT matrixA = (double*) malloc(sizeof(double) * N * M);
	double* DGEMM_RESTRICT matrixB = (double*) malloc(sizeof(double) * M * P);
	double* DGEMM_RESTRICT matrixC = (double*) malloc(sizeof(double) * N * P);


	double* DGEMM_RESTRICT temp = (double*) malloc(sizeof(double) * block_width * M);

	printf("Allocation complete, populating with values...\n");

	int i, j, k, r;

	#pragma omp parallel for
	for(i = 0; i < N; i++) {
		for(j = 0; j < M; j++) {
			matrixA[i * M + j] = 2.0;
		}
	}
	
	#pragma omp parallel for
	for(i = 0; i < M; i++) {
		for(j = 0; j < P; j++) {
			matrixB[i * P + j] = 0.5;
		}
	}
	#pragma omp parallel for
	for(i = 0; i < N; i++) {
		for(j = 0; j < P; j++) {
			matrixC[i * P + j] = 0.0;
		}
	}

	const double start = get_seconds();

	double *devPtrA, *devPtrB, *devPtrC; 
    cudaMalloc ((void**)&devPtrA, N * M * sizeof(double));
    cudaMalloc ((void**)&devPtrB, M * P * sizeof(double));
    cudaMalloc ((void**)&devPtrC, N * P * sizeof(double));

	cublasStatus_t cubstat;
	cublasHandle_t handle;
	cudaError_t cuberror;
	cubstat = cublasCreate(&handle);

	if( cubstat != CUBLAS_STATUS_SUCCESS ){ printf("HandleCreationFailure - 1 \n"); return 0; }
  
    cuberror = cudaMemcpy((void *)devPtrA, matrixA, N * M * sizeof(double), cudaMemcpyHostToDevice);
    if( cuberror != cudaSuccess ){ printf("cudaMemcpyFailure on devPtrA\n"); return 0; }

	cuberror = cudaMemcpy((void *)devPtrB, matrixB, M * P * sizeof(double), cudaMemcpyHostToDevice);
	if( cuberror != cudaSuccess ){ printf("cudaMemcpyFailure on devPtrB\n"); return 0; }
  
    cuberror = cudaMemcpy((void *)devPtrC, matrixC, N * P * sizeof(double), cudaMemcpyHostToDevice);
    if( cuberror != cudaSuccess ){ printf("cudaMemcpyFailure on devPtrC\n"); return 0; }


	// Repeat multiple times
	for(r = 0 ; r < repeats; r++) 
	{

        // GPU: cublasDgemm FUll XY
        
        cuberror = cudaMemcpy((void *)devPtrC, matrixC, N * P * sizeof(double), cudaMemcpyHostToDevice);
        if( cuberror != cudaSuccess ){ printf("cudaMemcpyFailure on devPtrC\n"); return 0; }

        tstart = get_seconds();

        cubstat = cublasDgemm( handle, CUBLAS_OP_N, CUBLAS_OP_N, P, N, M,
                    &alpha, devPtrB, P, devPtrA, M, &beta, devPtrC, P);
        cudaDeviceSynchronize();
        if(cubstat != CUBLAS_STATUS_SUCCESS){ printf("cublasDgemm Failed in Tiling\n"); return 0; }
        tend = get_seconds();
        
        cuberror = cudaMemcpy(matrixC, devPtrC, N * P * sizeof(double), cudaMemcpyDeviceToHost);
        if( cuberror != 0 ){ printf("cudaMemcpy failed devPtrC at blok_id: %d errocode: %d\n", i, cuberror);}
        cudaDeviceSynchronize();

        printf("Full cublasDgemm time: %.3lf\n", tend - tstart);

        sanity_check(matrixC, N, P, M * 2 * 0.50);
        set_zero_host_matrix(matrixC, N, P);

        cuberror = cudaMemcpy((void *)devPtrC, matrixC, N * P * sizeof(double), cudaMemcpyHostToDevice);
        if( cuberror != cudaSuccess ){ printf("cudaMemcpyFailure on devPtrC\n"); return 0; }


	    // GPU: cublasDgemm blocked XY

        printf("===============================================================\n");
        
        int nrowblk = ceil(1.0 * N/block_width);
        printf("            nrowblk =  %d\n", nrowblk);
        gpu_memory = 1e-9 * ((block_width * M * sizeof(double) + block_width * P * sizeof(double) + M * P * sizeof(double)));
        printf("    GPU memory used =  %0.3lf GB\n", gpu_memory);
        
        tstart = get_seconds();

        for(i = 0 ; i < nrowblk ; i++)
        {
            blksz = block_width;
            if(i * block_width + blksz > N)
                blksz = N - i * block_width;
            
            cubstat = cublasDgemm( handle, CUBLAS_OP_N, CUBLAS_OP_N, P, blksz, M,
                    &alpha, devPtrB, P, devPtrA+(i*block_width*M), M, &beta, devPtrC+(i*block_width*P), P);
            cudaDeviceSynchronize();
            if(cubstat != CUBLAS_STATUS_SUCCESS){ printf("cublasDgemm Failed in Tiling\n"); return 0; }
        }
        // cudaDeviceSynchronize();
        tend = get_seconds();
  
        cuberror = cudaMemcpy(matrixC, devPtrC, N * P * sizeof(double), cudaMemcpyDeviceToHost);
        if( cuberror != 0 ){ printf("cudaMemcpy failed devPtrC at blok_id: %d errocode: %d\n", i, cuberror);}
        cudaDeviceSynchronize();

	} // end of repeat loop

	

	double matrix_memory = 1e-9 * N * M * sizeof(double) + 1e-9 * M * P * sizeof(double) + 1e-9 * N * P * sizeof(double);
	const double time_taken = (tend - tstart);
	const double flops_computed = 1e-9 * 2 * N * M * P * (double)(repeats) + 1e-9 * 2 * N * P * (double)(repeats);
	
	printf("      Multiply time =  %0.3lf seconds\n", time_taken);
	printf("     FLOPs computed =  %0.3lf GF\n", flops_computed);
	printf("       GFLOP/s rate =  %0.3lf GF/s\n", flops_computed / time_taken);
	printf("Memory for Matrices =  %0.3lf GB\n", matrix_memory);

	printf("===============================================================\n");
	printf("\n");

	const double allowed_margin = 1.0e-8;
	for(i = 0 ; i < N ; i++)
	{
		for(j = 0 ; j < P ; j++)
		{
			if((matrixC[i * P + j] - M * 2 * 0.50) > allowed_margin)
			{
				printf(" -> Solution check FAILED.\n");
				return 0;
			}
		}
	}
	printf(" -> Solution check PASSED successfully.\n\n");
// #if defined(USE_CUBLAS)
// 	printf("%d,%d,%d,%d,%0.3lf,%0.3lf,%0.3lf,%0.3lf\n\n", N, M, P, block_width, matrix_memory, gpu_memory, time_taken, flops_computed / time_taken);
// #elif defined(USE_UM)
// 	printf("%d,%d,%d,%0.3lf,%0.3lf,%0.3lf\n\n", N, M, P, block_width, matrix_memory, time_taken, flops_computed / time_taken);
// #endif

    cudaFree (devPtrA);
	cudaFree (devPtrB);
	cudaFree (devPtrC);
	cublasDestroy(handle); 
	free(matrixA);
	free(matrixB);
	free(matrixC);

	return 0;
}
