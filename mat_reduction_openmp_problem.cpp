#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdio>
#include <iomanip>
#include <cmath>
#include <algorithm>
#include <sstream>
#include <cstring>
#include <fstream>
#include <vector>
#include <string>
using namespace std;

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <omp.h>


#include <cuda_runtime.h>

//rows
// #define N 41652230
// cols
// #define M 16

// int maxrow = 0, maxcol = 0;


double get_seconds() 
{
	struct timeval now;
	gettimeofday(&now, NULL);
	const double seconds = (double) now.tv_sec;
	const double usec    = (double) now.tv_usec;
	return seconds + (usec * 1.0e-6);
}

void sum_sqrt_column(double *src, double *dst, const int row, const int col)
{
    int i, j;
    
    #pragma omp parallel for default(shared) private(j)
    for(i = 0 ; i < col ; i++) //i->col
    {
        for(j = 0 ; j < row ; j++) //j->row
        {
            dst[i] += src[j * col + i];
        }
    }
}

void sum_sqrt_row(double *src, double *dst, const int row, const int col)
{
    int i, j;

    #pragma omp parallel for reduction(+:dst[:col])
    for (i = 0; i < row; i++)
    {
        for (j = 0 ; j < col; j++)
        {
            dst[j] += src[i * col + j];
        }
    }
}

void sum_sqrt_row_buffer(double *src, double *dst, const int row, const int col, int nbuf)
{
    int i, j;
    
    // int nthrd = omp_get_num_threads(); 
    
    int sz = nbuf * col;
    double *temp = (double *) malloc(sz * sizeof(double));
    
    // cout << "nbuf: " << nbuf << " col: " << col << endl;

    #pragma omp parallel for shared(temp)
    for (i = 0; i < sz; i++)
    {
        temp[i] = 0.0;
    }

    #pragma omp parallel for shared(temp) private(j)
    for (i = 0; i < row; i++)
    {
        for (j = 0 ; j < col; j++)
        {
            temp[omp_get_thread_num() * col + j] += src[i * col + j];
        }
    }

    for (i = 0; i < nbuf; i++)
    {
        for(j = 0 ; j < col ; j++)
        {
            dst[j] += temp[i * col + j];
        }
    }
}

void offload_testing(double *buf, int n)
{
    int i;

    #pragma omp target map(tofrom: buf[0 : n])
    #pragma omp teams distribute parallel for
    for(i = 0 ; i < n ; i++)
        buf[i] = 0.;
}

void sum_sqrt_row_gpu(double *src, double *dst, const int row, const int col, int nbuf)
{
    int i, j;

    int sz = nbuf * col;
    double *temp = (double *) malloc(sz * sizeof(double));
    
    // double *temp;
    // cudaMalloc((void**)&temp, sz);
    
    // cout << "nbuf: " << nbuf << " col: " << col << endl;

    #pragma omp parallel for shared(temp)
    for (i = 0; i < sz; i++)
    {
        temp[i] = 0.0;
    }

    // #pragma omp target is_device_ptr(temp)
    // #pragma omp parallel for shared(temp)
    // for (i = 0; i < sz; i++)
    // {
    //     temp[i] = 0.0;
    // }

    #pragma omp target is_device_ptr(src) map(tofrom: temp[0: sz])
    #pragma omp parallel for shared(temp) private(j)
    // #pragma omp teams distribute parallel for
    for (i = 0; i < row; i++)
    {
        for (j = 0 ; j < col; j++)
        {
            temp[omp_get_thread_num() * col + j] += src[i * col + j];
        }
    }

    #pragma omp target is_device_ptr(dst) map(to: temp[0: sz])
    {
    for (i = 0; i < nbuf; i++)
    {
        for(j = 0 ; j < col ; j++)
        {
            dst[j] += temp[i * col + j];
        }
    }
    }
}


void sum_sqrt_col_gpu(double *src, double *dst, const int row, const int col)
{
    int i, j;
    
    #pragma omp target is_device_ptr(src, dst)
    #pragma omp teams distribute parallel for //collapse(2) --> produce wrong result as expected
    // #pragma omp parallel for default(shared) private(j)
    for(i = 0 ; i < col ; i++) //i->col 
    {
        for(j = 0 ; j < row ; j++) //j->row
        {
            dst[i] += src[j * col + i];
        }
    }
}



void sum_sqrt_gpu_blocked(double *src, double *dst, int row, int col, int block_width, int block_id)
{
    int i, j, blksz;
    unsigned long int src_offset = block_id * block_width * col; 

    blksz = (block_id * block_width + block_width) > row ? row - block_id * block_width : block_width;

    #pragma omp target firstprivate(block_id, block_width, blksz, src_offset) is_device_ptr(src, dst) 
    #pragma omp teams distribute parallel for //collapse(2) --> produce wrong result as expected
    for(i = 0 ; i < col ; i++) //i->col
    {
        // #pragma omp simd
        for(j = 0 ; j < blksz ; j++) //j->row
        {
            dst[i] += src[src_offset + j * col + i];
        }
    }
}

// void sum_sqrt_gpu_row_blocked(double *src, double *dst, int row, int col, int block_width, int block_id)
// {
//     int i, j, blksz;
//     unsigned long int src_offset = block_id * block_width * col; 

//     blksz = (block_id * block_width + block_width) > row ? row - block_id * block_width : block_width;

//     #pragma omp target firstprivate(block_id, block_width, blksz, src_offset) is_device_ptr(src, dst) 
//     #pragma omp teams distribute parallel for
//     for(i = 0 ; i < col ; i++) //i->col
//     {
//         // #pragma omp simd
//         for(j = 0 ; j < blksz ; j++) //j->row
//         {
//             dst[i] += src[src_offset + j * col + i];
//         }
//     }
// }



int main(int argc, char *argv[]) 
{
    int N, M;

    int nthreads = 0;

    
    stringstream s(argv[1]);
    s >> N;
    stringstream s1(argv[2]);
    s1 >> M;
    // cout << "# of RHS Vectors: " << blocksize << endl;
    // cout << "Tile size: " << block_width << endl;

    cout << "row: " << N << " col: " << M << endl;

    double cons = 0.00075;
    double *dev_a, *dev_result;
    double *a, *result_col_wise, *result_row_wise;
    
    unsigned long int size = N * M * sizeof(double);

    a = (double *) malloc(size);
    result_col_wise = (double *) malloc(M * sizeof(double));
    result_row_wise = (double *) malloc(M * sizeof(double));
    
    for(int i = 0 ; i < N ; i++)
    {    
        for (int j = 0 ; j < M ; j++)
        {
            a[i * M + j] = cons;
        }
    }

    for(int i = 0 ; i < M ; i++)
    {
        result_col_wise[i] = 0;
        result_row_wise[i] = 0;
    }

    #pragma omp parallel
    {
        #pragma omp master
        {
            nthreads = omp_get_num_threads();
        }
    }

    // VERSION: CPU( OMP)
    double tstart = get_seconds();

    sum_sqrt_column(a, result_col_wise, N, M);
    
    double tend = get_seconds();
    double cpu_col_wise_time = tend - tstart;

    
    
    double val = N * cons;
    bool isMatched = true;
    // // printf("value: %lf\n", value);

    for(int i = 0 ; i < M ; i++)
    {
        if(abs(result_col_wise[i] - val) > 0.01)
        {
            isMatched = false;
            break;
        }
    }
    
    if(isMatched)
        printf("cpu-col-wise: SUCCESSFUL :)\n");
    else
    printf("cpu-col-wise: UNSUCCESSFUL :(\n");

    
    /*
    // cpu row wise

    tstart = get_seconds();

    sum_sqrt_row_buffer(a, result_row_wise, N, M, nthreads);
    
    tend = get_seconds();
    double cpu_row_wise_time = tend - tstart;
    
    
    isMatched = true;
    // // printf("value: %lf\n", value);

    for(int i = 0 ; i < M ; i++)
    {
        // printf("%lf ", result_row_wise[i]);
        if(abs(result_row_wise[i] - val) > 0.01)
        {
            isMatched = false;
            break;
        }
    }
    // printf("\n");

    if(isMatched)
        printf("cpu-row-wise: SUCCESSFUL :)\n");
    else
    printf("cpu-row-wise: UNSUCCESSFUL :(\n"); */

     for(int i = 0 ; i < M ; i++)
    {
        result_col_wise[i] = 0.0;
        result_row_wise[i] = 0.0;
    }

    // printf("cpu-col-wise time: %.3lf sec.\n", cpu_col_wise_time);
    // printf("cpu-row-wise time: %.3lf sec.\n", cpu_row_wise_time);

    
    /*
    // gpu row wise

    cudaMalloc((void**)&dev_a, size);
    cudaMalloc((void**)&dev_result, M * sizeof(double));
    
    cudaMemcpy(dev_a, a, size, cudaMemcpyHostToDevice);
    cudaMemcpy(dev_result, result_row_wise, M * sizeof(double), cudaMemcpyHostToDevice);

    
    tstart = get_seconds();

    sum_sqrt_row_gpu(dev_a, dev_result, N, M, 128);
    cudaDeviceSynchronize();
    
    tend = get_seconds();
    double gpu_row_wise_time = tend - tstart;
    

    double *result_row_gpu = (double *) malloc(M * sizeof(double));

    cudaMemcpy(result_row_gpu, dev_result, M * sizeof(double), cudaMemcpyDeviceToHost);
    
    isMatched = true;
    // printf("value: %lf\n", val);

    for(int i = 0 ; i < M ; i++)
    {
        // printf("%lf ", result_row_gpu[i]);
        
        // cout << i << " : " << result_row_gpu[i] << " " << val << " " << abs(result_row_gpu[i] - val) << endl;;
        // cout << abs(result_row_gpu[i] - val) << " " << endl;

        if(abs(result_row_gpu[i] - val) > 0.01)
        {
            isMatched = false;
            break;
        }
    }
    // printf("\n");

    if(isMatched)
        printf("gpu-row-wise: SUCCESSFUL :)\n");
    else
    printf("gpu-row-wise: UNSUCCESSFUL :(\n");

    printf("gpu-row-wise time: %.3lf sec.\n", gpu_row_wise_time); */

    
    // gpu col wise
   
    cudaMemcpy(dev_result, result_row_wise, M * sizeof(double), cudaMemcpyHostToDevice);

    tstart = omp_get_wtime(); //get_seconds();

    sum_sqrt_col_gpu(dev_a, dev_result, N, M);
    cudaDeviceSynchronize();
    
    tend = omp_get_wtime();
    double gpu_col_wise_time = tend - tstart;
    

    double *result_col_gpu = (double *) malloc(M * sizeof(double));

    cudaMemcpy(result_col_gpu, dev_result, M * sizeof(double), cudaMemcpyDeviceToHost);
    
    isMatched = true;
    
    for(int i = 0 ; i < M ; i++)
    {
        if(abs(result_col_gpu[i] - val) > 0.01)
        {
            isMatched = false;
            break;
        }
    }
    printf("\n");

    if(isMatched)
        printf("gpu-col-wise: SUCCESSFUL :)\n");
    else
    printf("gpu-col-wise: UNSUCCESSFUL :(\n");

        

    // gpu blocked col
    int block_width = 262144;
    int nrowblk = ceil(1.0 * N/block_width);
    

    cudaMemcpy(dev_result, result_row_wise, M * sizeof(double), cudaMemcpyHostToDevice);


    tstart = get_seconds();

    for(int i = 0 ; i < nrowblk ; i++)
    {
        sum_sqrt_gpu_blocked(dev_a, dev_result, N, M, block_width, i);
        cudaDeviceSynchronize();
    }
    
    tend = get_seconds();
    double gpu_blocked_time = tend - tstart;
    

    double *result_blocked_gpu = (double *) malloc(M * sizeof(double));

    cudaMemcpy(result_blocked_gpu, dev_result, M * sizeof(double), cudaMemcpyDeviceToHost);
    
    isMatched = true;
    
    for(int i = 0 ; i < M ; i++)
    {
        if(abs(result_blocked_gpu[i] - val) > 0.01)
        {
            isMatched = false;
            break;
        }
    }
    printf("\n");

    if(isMatched)
        printf("gpu-blocked: SUCCESSFUL :)\n");
    else
    printf("gpu-blocked: UNSUCCESSFUL :(\n");


    printf("\n------------------------------------\n");
    printf("cpu-col-wise time: %.3lf sec.\n", cpu_col_wise_time);
    // printf("cpu-row-wise time: %.3lf sec.\n", cpu_row_wise_time);
    // printf("gpu-row-wise time: %lf sec.\n", gpu_row_wise_time);
    printf("gpu-col-wise time: %lf sec.\n", gpu_col_wise_time);
    printf("gpu-blocked time:  %lf sec.\n", gpu_blocked_time);


    // cout << "nrowblk: " << nrowblk << endl; 
    // printf("nthreads : %d\n", nthreads);
    return 0;
}

