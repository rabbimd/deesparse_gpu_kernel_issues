#!/bin/bash -l
module purge
module load esslurm
module load PrgEnv-llvm/11.0.0-git_20200409
module list
set -x

make -f Makefile_clang clean
make -f Makefile_clang xty

export OMP_NUM_THREADS=5


# XY

# srun -n 1 -c 40 --cpu_bind=cores ./xty_mini_app.x 100000000 8 8 65536
# srun -n 1 -c 40 --cpu_bind=cores ./xty_mini_app.x 100000000 8 8 131072
srun -n 1 -c 10 --cpu_bind=cores ./xty_mini_app.x 100000000 8 8 262144
# srun -n 1 -c 40 --cpu_bind=cores ./xty_mini_app.x 100000000 8 8 524288
# srun -n 1 -c 40 --cpu_bind=cores ./xty_mini_app.x 100000000 8 8 1048576

# srun -n 1 -c 40 nsys profile -t cuda,osrt,nvtx,cublas -f true -o xy_clang.qdstrm ./xy_mini_app.x 100000000 8 8 1048576

# srun -n 1 -c 40 --cpu_bind=cores ./xty_mini_app.x 50000000 8 8 65536
# srun -n 1 -c 40 --cpu_bind=cores ./xty_mini_app.x 50000000 8 8 131072
# srun -n 1 -c 40 --cpu_bind=cores ./xty_mini_app.x 50000000 8 8 262144
# srun -n 1 -c 40 --cpu_bind=cores ./xty_mini_app.x 50000000 8 8 524288
# srun -n 1 -c 40 --cpu_bind=cores ./xty_mini_app.x 50000000 8 8 1048576


# srun -n 1 -c 10 --cpu_bind=cores ./xty_mini_app.x 50000000 16 16 262144
# srun -n 1 -c 10 --cpu_bind=cores ./xty_mini_app.x 100000000 8 8 262144
# srun -n 1 -c 10 --cpu_bind=cores ./xty_mini_app.x 41652230 8 8 262144

