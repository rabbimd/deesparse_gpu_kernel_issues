# DeeSparse_GPU_Kernel_Issues
## System
I have used Cori-GPU testbed @ NERSC for testing the following programs.

## Compiling SpMM mini app
./SpMM_mini_app.x [# of RHS vector] [tile size] [path to Sparse Matrix stored in CSR format]\
./test_clang.sh --> this will load necessary modules for clang compiler first then compile and run SpMM_mini_app.cpp file.\
./test_cray.sh --> this will load necessary modules for cray-classic compiler first then compile and run SpMM_mini_app.cpp file.

## Compiling XY mini app
./test_clang_xy.sh --> this will load necessary modules for clang compiler first then compile and run xy_mini_app.cpp file.\
./test_cray_xy.sh --> this will load necessary modules for cray-classic compiler first then compile and run xy_mini_app.cpp file.

## Compiling XY mini app
./test_clang_xy.sh --> this will load necessary modules for clang compiler first then compile and run xy_mini_app.cpp file.\
./test_cray_xy.sh --> this will load necessary modules for cray-classic compiler first then compile and run xy_mini_app.cpp file.

## Compiling Reduction kernel
./test_clang_reduction.sh --> this will load necessary modules for clang compiler first then compile and run mat_reduction_openmp.cpp file.\
./test_cray_reduction.sh --> this will load necessary modules for cray-classic compiler first then compile and run mat_reduction_openmp.cpp file.

## CUDA reduction

nvcc cuda_reduction.cu -o cuda_reduction.x -arch=sm_70\
srun -n 1 -c 1 ./cuda_reduction.x

