
#include <ctime>
#include<cmath>
#include <iostream>

using namespace std;

#include <omp.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>

#define N 128568730
#define M 8


double get_seconds();
void reduction_cpu(double *src, double *dst, const int row, const int col);
void reduction_gpu(double *src, double *dst, const int row, const int col);
void reduction_gpu_blocked(double *src, double *dst, int row, int col, int block_width, int block_id);

void reduction_gpu_v1(double *src, double *dst, const int row, const int col);

int main(int argc, char *argv[]) 
{
    // int N, M;
    // int N = atoi(argv[1]); // <-- cray-classic compiler causes a significant increase in exectuion time of function reduction_gpu() for this conversion (WEIRD!!)
    // int M = atoi(argv[2]);
    // int block_width = atoi(argv[3]);
    int block_width = 262144;

    cout << "# of row: " << N << endl << "# of col: " << M << endl << "Tile size: " << block_width << endl;

    double cons = 0.00075;
    double *dev_a, *dev_result;
    double *a, *result_col_wise, *result_row_wise;
    
    unsigned long int size = N * M * sizeof(double);

    a = (double *) malloc(N * M * sizeof(double));
    result_col_wise = (double *) malloc(M * sizeof(double));
    result_row_wise = (double *) malloc(M * sizeof(double));
    
    for(int i = 0 ; i < N ; i++)
    {    
        for (int j = 0 ; j < M ; j++)
        {
            a[i * M + j] = cons;
        }
    }

    for(int i = 0 ; i < M ; i++)
    {
        result_col_wise[i] = 0;
        result_row_wise[i] = 0;
    }

    // -----------------------------------------------------------------------------------------------------
    // VERSION: CPU (OMP)
    double tstart = get_seconds();

    reduction_cpu(a, result_col_wise, N, M);
    
    double tend = get_seconds();
    double cpu_col_wise_time = tend - tstart;

    // sanity check
    double val = N * cons;
    bool isMatched = true;
    for(int i = 0 ; i < M ; i++)
    {
        if(abs(result_col_wise[i] - val) > 0.01)
        {
            isMatched = false;
            break;
        }
    }
    if(isMatched)
        printf("         CPU (OMP): SUCCESSFUL :)\n");
    else
        printf("         CPU (OMP): UNSUCCESSFUL :(\n");
    // allocating memory on GPU
    cudaMalloc((void**)&dev_a, N * M * sizeof(double));
    cudaMalloc((void**)&dev_result, M * sizeof(double));
    
    cudaMemcpy(dev_a, a, N * M * sizeof(double), cudaMemcpyHostToDevice);
    
    

    // -----------------------------------------------------------------------------------------------------
    // VERSION: OMPTarget

    for(int i = 0 ; i < M ; i++)
    {
        result_col_wise[i] = 0.0;
    }

    cudaMemcpy(dev_result, result_col_wise, M * sizeof(double), cudaMemcpyHostToDevice);

    tstart = get_seconds(); 

    reduction_gpu(dev_a, dev_result, N, M);
    cudaDeviceSynchronize();
    
    tend = get_seconds();
    double gpu_col_wise_time = tend - tstart;
    
    // sanity check
    double *result_col_gpu = (double *) malloc(M * sizeof(double));
    cudaMemcpy(result_col_gpu, dev_result, M * sizeof(double), cudaMemcpyDeviceToHost);
    isMatched = true;
    for(int i = 0 ; i < M ; i++)
    {
        if(abs(result_col_gpu[i] - val) > 0.01)
        {
            isMatched = false;
            break;
        }
    }
    if(isMatched)
        printf("         OMPTarget: SUCCESSFUL :)\n");
    else
        printf("         OMPTarget: UNSUCCESSFUL :)\n");



    // reduction_gpu_v1
    cudaMemset(dev_result, 0, M * sizeof(double));

    tstart = get_seconds(); 

    reduction_gpu_v1(dev_a, dev_result, N, M);
    cudaDeviceSynchronize();
    
    tend = get_seconds();
    //double gpu_col_wise_time = tend - tstart;
    cout << "reduction_gpu_v1 time: " << get_seconds() - tstart << " sec." << endl;
    // sanity check
    // double *result_col_gpu = (double *) malloc(M * sizeof(double));
    cudaMemcpy(result_col_gpu, dev_result, M * sizeof(double), cudaMemcpyDeviceToHost);
    isMatched = true;
    for(int i = 0 ; i < M ; i++)
    {
        if(abs(result_col_gpu[i] - val) > 0.01)
        {
            isMatched = false;
            break;
        }
    }
    if(isMatched)
        printf(" reduction_gpu_v1        OMPTarget: SUCCESSFUL :)\n");
    else
        printf("  reduction_gpu_v1       OMPTarget: UNSUCCESSFUL :)\n");

        
    // -----------------------------------------------------------------------------------------------------
    // VERSION: OMPTarget, blocked
    int nrowblk = ceil(1.0 * N/block_width);
    cudaMemcpy(dev_result, result_row_wise, M * sizeof(double), cudaMemcpyHostToDevice);

    tstart = get_seconds();
    for(int i = 0 ; i < nrowblk ; i++)
    {
        reduction_gpu_blocked(dev_a, dev_result, N, M, block_width, i);
        // cudaDeviceSynchronize();
    }
    cudaDeviceSynchronize();
    tend = get_seconds();
    double gpu_blocked_time = tend - tstart;
    
    // sanity check
    double *result_blocked_gpu = (double *) malloc(M * sizeof(double));
    cudaMemcpy(result_blocked_gpu, dev_result, M * sizeof(double), cudaMemcpyDeviceToHost);
    isMatched = true;
    for(int i = 0 ; i < M ; i++)
    {
        if(abs(result_blocked_gpu[i] - val) > 0.01)
        {
            isMatched = false;
            break;
        }
    }

    if(isMatched)
        printf("OMPTarget, blocked: SUCCESSFUL :)\n\n");
    else
        printf("OMPTarget, blocked: UNSUCCESSFUL :(\n\n");

    
    
    

    printf("--------------------------------------------\n");
    printf("         CPU (OMP) time: %.3lf sec.\n", cpu_col_wise_time);
    printf("         OMPTarget time: %lf sec.\n", gpu_col_wise_time);
    printf("OMPTarget, blocked time: %lf sec.\n", gpu_blocked_time);

    return 0;
}

void reduction_cpu(double *src, double *dst, const int row, const int col)
{
    int i, j;
    
    #pragma omp parallel for default(shared) private(j)
    for(i = 0 ; i < col ; i++) //i->col
    {
        for(j = 0 ; j < row ; j++) //j->row
        {
            dst[i] += src[j * col + i];
        }
    }
}

void reduction_gpu(double *src, double *dst, const int row, const int col)
{
    int i, j;
    
    #pragma omp target is_device_ptr(src, dst)
    #pragma omp teams distribute parallel for //collapse(2) //try collapse to see if it works
    // #pragma omp parallel for default(shared) private(j)
    for(i = 0 ; i < col ; i++) //i->col 
    {
        for(j = 0 ; j < row ; j++) //j->row
        {
            dst[i] += src[j * col + i];
        }
    }
}

// user defined array section reduction: https://stackoverflow.com/questions/20413995/reducing-on-array-in-openmp


void reduction_gpu_v1(double *src, double *dst, const int row, const int col)
{
    int i, j;
    int count = 0;
    
    #pragma omp target teams is_device_ptr(src, dst)
    for(i = 0 ; i < col ; i++) //i->col 
    {
        #pragma omp distribute parallel for
        for(j = 0 ; j < row ; j++) //j->row
        {
            #pragma omp atomic
            dst[i] += src[j * col + i];
        }
    }
}


// void reduction_gpu_blocked_prev(double *src, double *dst, int row, int col, int block_width, int block_id)
// {
//     int i, j, blksz;
//     unsigned long int src_offset = block_id * block_width * col; 

//     blksz = (block_id * block_width + block_width) > row ? row - block_id * block_width : block_width;

//     #pragma omp target firstprivate(block_id, block_width, blksz, src_offset) is_device_ptr(src, dst) 
//     #pragma omp teams distribute parallel for //collapse(2)
//     for(i = 0 ; i < col ; i++) //i->col
//     {
//         // #pragma omp simd
//         for(j = 0 ; j < blksz ; j++) //j->row
//         {
//             dst[i] += src[src_offset + j * col + i];
//         }
//     }
// }


void reduction_gpu_blocked(double *src, double *dst, int row, int col, int block_width, int block_id)
{
    int i, j, blksz;
    unsigned long int src_offset = block_id * block_width * col; 

    blksz = (block_id * block_width + block_width) > row ? row - block_id * block_width : block_width;

    // teams --> maps to SM
    #pragma omp target teams distribute is_device_ptr(src, dst) nowait //firstprivate(block_id, block_width, blksz, src_offset) 
    for(i = 0 ; i < col ; i++) //i->col
    {
        // --> maps to computational unit (thread blocks)
        #pragma omp parallel for //collapse(2)
        for(j = 0 ; j < blksz ; j++) //j->row
        {
            #pragma omp atomic
            dst[i] += src[src_offset + j * col + i];
        }
    }
}

double get_seconds() 
{
	struct timeval now;
	gettimeofday(&now, NULL);
	const double seconds = (double) now.tv_sec;
	const double usec    = (double) now.tv_usec;
	return seconds + (usec * 1.0e-6);
}


// the follwing kernels are not used in this mini app --> here for testing purpose

/*
void sum_sqrt_row(double *src, double *dst, const int row, const int col)
{
    int i, j;

    #pragma omp parallel for reduction(+:dst[:col])
    for (i = 0; i < row; i++)
    {
        for (j = 0 ; j < col; j++)
        {
            dst[j] += src[i * col + j];
        }
    }
}

void sum_sqrt_row_buffer(double *src, double *dst, const int row, const int col, int nbuf)
{
    int i, j;
    
    // int nthrd = omp_get_num_threads(); 
    
    int sz = nbuf * col;
    double *temp = (double *) malloc(sz * sizeof(double));
    
    cout << "nbuf: " << nbuf << " col: " << col << endl;

    #pragma omp parallel for shared(temp)
    for (i = 0; i < sz; i++)
    {
        temp[i] = 0.0;
    }

    #pragma omp parallel for shared(temp) private(j)
    for (i = 0; i < row; i++)
    {
        for (j = 0 ; j < col; j++)
        {
            temp[omp_get_thread_num() * col + j] += src[i * col + j];
        }
    }

    for (i = 0; i < nbuf; i++)
    {
        for(j = 0 ; j < col ; j++)
        {
            dst[j] += temp[i * col + j];
        }
    }
}

void offload_testing(double *buf, int n)
{
    int i;

    #pragma omp target map(tofrom: buf[0 : n])
    #pragma omp teams distribute parallel for
    for(i = 0 ; i < n ; i++)
        buf[i] = 0.;
}

void sum_sqrt_row_gpu(double *src, double *dst, const int row, const int col, int nbuf)
{
    int i, j;

    int sz = nbuf * col;
    double *temp = (double *) malloc(sz * sizeof(double));
    
    // double *temp;
    // cudaMalloc((void**)&temp, sz);
    
    cout << "nbuf: " << nbuf << " col: " << col << endl;
     

    #pragma omp parallel for shared(temp)
    for (i = 0; i < sz; i++)
    {
        temp[i] = 0.0;
    }

    // #pragma omp target is_device_ptr(temp)
    // #pragma omp parallel for shared(temp)
    // for (i = 0; i < sz; i++)
    // {
    //     temp[i] = 0.0;
    // }

    #pragma omp target is_device_ptr(src) map(tofrom: temp[0: sz])
    // #pragma omp parallel for shared(temp) private(j)
    #pragma omp teams distribute parallel for
    for (i = 0; i < row; i++)
    {
        for (j = 0 ; j < col; j++)
        {
            temp[omp_get_thread_num() * col + j] += src[i * col + j];
        }
    }

    #pragma omp target is_device_ptr(dst) map(to: temp[0: sz])
    {
    for (i = 0; i < nbuf; i++)
    {
        for(j = 0 ; j < col ; j++)
        {
            dst[j] += temp[i * col + j];
        }
    }
    }
}

void sum_sqrt_gpu_row_blocked(double *src, double *dst, int row, int col, int block_width, int block_id)
{
    int i, j, blksz;
    unsigned long int src_offset = block_id * block_width * col; 

    blksz = (block_id * block_width + block_width) > row ? row - block_id * block_width : block_width;

    #pragma omp target firstprivate(block_id, block_width, blksz, src_offset) is_device_ptr(src, dst) 
    #pragma omp teams distribute parallel for
    for(i = 0 ; i < col ; i++) //i->col
    {
        // #pragma omp simd
        for(j = 0 ; j < blksz ; j++) //j->row
        {
            dst[i] += src[src_offset + j * col + i];
        }
    }
}
*/
