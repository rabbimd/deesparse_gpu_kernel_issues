#!/bin/bash -l
module purge
module load esslurm
module load cdt/19.11
module load PrgEnv-cray
module switch cce cce/9.1.0-classic
# module switch cce cce/9.1.3-classic
module load craype-x86-skylake
module unload cray-libsci
module load cudatoolkit craype-accel-nvidia70
module load cuda
#module load nsight-systems
#module load cuda/10.1.168

module list
set -x

make -f Makefile_cray_classic clean
make -f Makefile_cray_classic xty

export OMP_NUM_THREADS=5

# XTY

# srun -n 1 -c 40 --cpu_bind=cores ./xty_mini_app.x 100000000 8 8 65536
# srun -n 1 -c 40 --cpu_bind=cores ./xty_mini_app.x 100000000 8 8 131072
srun -n 1 -c 10 --cpu_bind=cores ./xty_mini_app.x 100000000 8 8 262144
# srun -n 1 -c 40 --cpu_bind=cores ./xty_mini_app.x 100000000 8 8 524288
# srun -n 1 -c 40 --cpu_bind=cores ./xty_mini_app.x 100000000 8 8 1048576


# srun -n 1 -c 40 nsys profile -t cuda,osrt,nvtx,cublas -f true -o xy_cray.qdstrm ./xty_mini_app.x 100000000 8 8 1048576
