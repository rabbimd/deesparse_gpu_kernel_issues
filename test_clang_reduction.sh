#!/bin/bash

module purge
module load PrgEnv-llvm

set -x

export OMP_NUM_THREADS=5
export OMP_PLACES=cores 
export OMP_PROC_BIND=close

rm *.x
clang++ -fopenmp -O3 -fopenmp-targets=nvptx64-nvidia-cuda mat_reduction_openmp.cpp -o mat_reduction_openmp.x -I${CUDA_ROOT}/include -L${CUDA_ROOT}/lib64 -lcudart

srun -n 1 -c 10 --cpu_bind=cores ./mat_reduction_openmp.x