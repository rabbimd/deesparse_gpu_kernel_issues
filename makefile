CC=clang++
CFLAGS=-fopenmp -O3 #-m64 #-I${MKL_ROOT}/include
# OFFLOADFLAG=-foffload=nvptx-none="-Ofast -lm -misa=sm_35"
OFFLOADFLAG=-fopenmp-targets=nvptx64-nvidia-cuda
#CFLAGS+=-DSHORT_INT
CFLAGS_EXTRA=-Wall -Wextra -Wconversion
#CC=icpc
#CFLAGS=-std=c++11 -Wall -Wextra -Wconversion -O3  -qopenmp
#CFLAGS= -march=knl -fopenmp -O3 -std=c++11 -m64 -I${MKL_ROOT}/include
#CFLAGS= -xMIC-AVX512 -qopenmp -O3 -std=c++11 -m64 -I${MKL_ROOT}/include
#CFLAGS= -qopenmp -O3 -std=c++11 -m64 -I${MKL_ROOT}/include
LINKER_INT32=-L${MKL_ROOT}/lib/intel64 -Wl,--no-as-needed -lmkl_intel_lp64 -lmkl_gnu_thread -lmkl_core -lgomp -lpthread -lm -ldl

EXEDIR=.


lobpcg_libcsr: 
	$(CC) $(CFLAGS) $(OFFLOADFLAG) mat_reduction_openmp.cpp -o mat_reduction_openmp.x -I${CUDA_ROOT}/include -L${CUDA_ROOT}/lib64 -lcublas -lcudart -lcusparse

