#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdio>
#include <iomanip>
#include <cmath>
#include <algorithm>
#include <sstream>
#include <cstring>
#include <fstream>
#include <vector>
#include <string>
using namespace std;

#include <omp.h>
#include <cuda_runtime.h>
#include "cublas_v2.h"
#include "cusparse.h"

#if defined(NSYS)
#include <nvToolsExt.h>
#endif

#define CHECK_CUDA(func)                                                       \
{                                                                              \
    cudaError_t status = (func);                                               \
    if (status != cudaSuccess) {                                               \
        printf("CUDA API failed at line %d with error: %s (%d)\n",             \
               __LINE__, cudaGetErrorString(status), status);                  \
        return EXIT_FAILURE;                                                   \
    }                                                                          \
}

#define CHECK_CUSPARSE(func)                                                   \
{                                                                              \
    cusparseStatus_t status = (func);                                          \
    if (status != CUSPARSE_STATUS_SUCCESS) {                                   \
        printf("CUSPARSE API failed at line %d with error: %s (%d)\n",         \
               __LINE__, cusparseGetErrorString(status), status);              \
        return EXIT_FAILURE;                                                   \
    }                                                                          \
}


int numrows, numcols, nnonzero, nthrds;
int *ia, *ja;
double *acsr;

int numrows_coo, numcols_coo, nnz_coo;
int *rowind, *colind;
double *value_coo;

void read_csr(char *filename);
void read_coo(char *filename);
void spmm_csr(int row, int col, int nvec, int *row_ptr, int *col_index, double *value, double *Y, double *result);
void spmm_csr_gpu(int numrows, int numcols, int nvec, int *row_ptr, int *col_index, double *value, double *d_Y, double *Z);
void spmm_coo_gpu(int *rowind, int *colind, double *value, double *d_Y, double *Z, int numrows, int numcols, int nvec, int nnz);
void transpose_GPU(double *src, double *dst, const int N, const int M);
void matrix_l2_norm_col_wise(double *arr, double *result, int row, int col);
void SpMM_cuSparse(int *d_ia, int *d_ja, double *d_acsr, int numrows, int numcols, int nnonzero, int blocksize, 
                    double *d_blockVectorX, double *d_blockVectorR);
void SpMM_cuSparse_blocked(int *d_ia, int *d_ja, double *d_acsr, int numrows, int numcols, int nnonzero, int blocksize, 
                    double *d_blockVectorX, double *d_blockVectorR, int block_id, int block_width, int *nnz_per_tile);

void SPMM_GPU_blocked(int *row_ptr, int *col_index, double *value, double *d_Y, double *Z, 
                        int numrows, int numcols, int nvec, int block_width, int block_id);
int main(int argc, char *argv[])
{
    int M, N, blocksize;
    int i, j, k;
    double tstart, tend, total_device_mem = 0;
    string str;
    int block_width; 

    stringstream s(argv[1]);
    s >> blocksize;
    stringstream s1(argv[2]);
    s1 >> block_width;
    cout << "# of RHS Vectors: " << blocksize << endl;
    cout << "Tile size: " << block_width << endl;

    
    char *filename = argv[3] ; 
   

    tstart = omp_get_wtime();
    // reading sparse matrix in CSR format for file 
    read_csr(filename); 
    tend = omp_get_wtime(); 
    cout << "CSR file reading time: " << tend - tstart << " sec." << endl;
    
    #pragma omp parallel shared(nthrds)
    #pragma omp master
    {
        nthrds = omp_get_num_threads();
    }

    cout << "# of CPU threads: " << nthrds << endl;
    int nthrds_gpu = 128;    
    
    M = numrows;
    N = numcols;

    double *blockVectorX = (double *) malloc(N * blocksize * sizeof(double)); 
    double *blockVectorR = (double *) malloc(N * blocksize * sizeof(double));

    double *blockVectorR_cpu = (double *) malloc(N * blocksize * sizeof(double));
    double *h_blockVectorR = (double *) malloc(N * blocksize * sizeof(double));

    // double *h_blockVectorR_xt = (double *) malloc(M * blocksize * sizeof(double)); 
    // double *h_blockVectorR_coo = (double *) malloc(M * blocksize * sizeof(double)); 

    srand(0);
   //#pragma omp parallel for private(j) default(shared)
    for(i = 0 ; i < N ; i++)
    {
        for(j = 0 ; j < blocksize ; j++)
        {
            blockVectorX[i * blocksize + j] = 0.00000001; //(double)rand()/(double)RAND_MAX; //0.00000001; //(double)rand()/(double)RAND_MAX;
            
            blockVectorR[i * blocksize + j] = 0.0;
            h_blockVectorR[i * blocksize + j] = 0.0;
            blockVectorR_cpu[i * blocksize + j] = 0.0;
            
            // h_blockVectorR_coo[i * blocksize + j] = 0.0;
            // h_blockVectorR_xt[i * blocksize + j] = 0.0;
        }
    }

    int h, t;
    h = omp_get_initial_device();
    t = omp_get_default_device();
    
    int status; 
    const double cudaAlpha = 1.0;
    const double cudaBeta = 0.0;
    const double cudaBetaOne = 1.0;
        
    cudaError_t cuberror;
    total_device_mem = (2 * N * blocksize + nnonzero )* sizeof(double) * 1e-9 + (nnonzero + numrows + 1) * sizeof(int) * 1e-9;
    cout << "Total Device memory: " << total_device_mem << " GB" << endl;

    // allocating memory on GPU
    double *d_blockVectorX = (double *) omp_target_alloc(N * blocksize * sizeof(double), t);;
    if(d_blockVectorX == NULL){ printf("omp_target_alloc Filed d_blockVectorX\n"); return 0; }

    double *d_blockVectorR = (double *) omp_target_alloc(N * blocksize * sizeof(double), t);;
    if(d_blockVectorR == NULL){ printf("omp_target_alloc Filed d_blockVectorR\n"); return 0; }

    // double *d_temp = (double *) omp_target_alloc(N * blocksize * sizeof(double), t);;
    // if(d_temp == NULL){ printf("omp_target_alloc Filed d_temp\n"); return 0; }

    int *d_ia = (int *) omp_target_alloc((numrows + 1) * sizeof(int), t);;
    if(d_ia == NULL){ printf("omp_target_alloc Filed d_ia\n"); return 0; }

    int *d_ja = (int *) omp_target_alloc(nnonzero * sizeof(int), t);;
    if(d_ja == NULL){ printf("omp_target_alloc Filed d_ja\n"); return 0; }

    double *d_acsr = (double *) omp_target_alloc(nnonzero * sizeof(double), t);;
    if(d_acsr == NULL){ printf("omp_target_alloc Filed d_acsr\n"); return 0; }

    // trasnfering necessary data to gpu

    tstart = omp_get_wtime();

    cuberror = cudaMemcpy(d_blockVectorX, blockVectorX, N * blocksize * sizeof(double), cudaMemcpyHostToDevice);
    cuberror = cudaMemcpy(d_blockVectorR, blockVectorR, N * blocksize * sizeof(double), cudaMemcpyHostToDevice);
    cudaError_t cuberror1 = cudaMemcpy(d_ia, ia, (numrows + 1) * sizeof(int), cudaMemcpyHostToDevice);
    cudaError_t cuberror2 = cudaMemcpy(d_ja, ja, nnonzero * sizeof(int), cudaMemcpyHostToDevice);
    cudaError_t cuberror3 = cudaMemcpy(d_acsr, acsr, nnonzero * sizeof(double), cudaMemcpyHostToDevice);
    cudaDeviceSynchronize();
    
    if(cuberror != 0){ printf("cudaMemcpy failed d_blockVectorX: %d\n", cuberror);}
    if(cuberror1 != 0){ printf("cudaMemcpy failed d_ia: %d\n", cuberror1);}
    if(cuberror2 != 0){ printf("cudaMemcpy failed d_ja: %d\n", cuberror2);}
    if(cuberror3 != 0){ printf("cudaMemcpy failed d_acsr: %d\n", cuberror3);}
    
    tend = omp_get_wtime();
    // cout << "H2D Time: " << tend - tstart << " sec." << endl;

    // ------------------------------------------------------------------------------------------------------

    // VERSION: OMP (CPU)

    double *seq_norm = (double *) malloc(blocksize * sizeof(double));
    for(i = 0 ; i < blocksize ; i++)
        seq_norm[i] = 0.0;


    tstart = omp_get_wtime();
    spmm_csr(numrows, numcols, blocksize, ia, ja, acsr, blockVectorX, blockVectorR_cpu);
    tend = omp_get_wtime();
    double cpu_time = tend - tstart;
    // matrix_l2_norm_col_wise(blockVectorR_cpu, seq_norm, N, blocksize);

    // for(i = 0 ; i < blocksize ; i++)
    //     cout << seq_norm[i] << " ";
    // cout << endl;

    

    // ------------------------------------------------------------------------------------------------------

    double *gpu_norm = (double *) malloc(blocksize * sizeof(double));
    double *gpu_norm_blocked = (double *) malloc(blocksize * sizeof(double));
    for(i = 0 ; i < blocksize ; i++)
    {
        gpu_norm[i] = 0.0;
        gpu_norm_blocked[i] = 0.0;
    }


    double *g_norm = (double *) omp_target_alloc(blocksize * sizeof(double), t);;
    if(g_norm == NULL){ printf("omp_target_alloc Filed g_norm\n"); return 0; }

    double *g_norm_blocked = (double *) omp_target_alloc(blocksize * sizeof(double), t);;
    if(g_norm_blocked == NULL){ printf("omp_target_alloc Filed g_norm_blocked\n"); return 0; }

    #pragma omp target is_device_ptr(g_norm, g_norm_blocked)
    #pragma omp parallel for
    for(i = 0 ; i < blocksize ; i++)
    {
        g_norm[i] = 0.0;
        g_norm_blocked[i] = 0.0;
    }


    // int block_width = 262144;

    int nrowblk = ceil(1.0 * numrows/block_width);
    int *nnz_per_tile = (int *)malloc(nrowblk * sizeof(int));
    int max_nnz = 0;
    
    printf("               # of tile =  %d\n", nrowblk);
    int totalnnz = 0;
    
    for(i = 0 ; i < nrowblk; i++)
    {
        if(i < nrowblk - 1)
            nnz_per_tile[i] = ia[(i + 1) * block_width] - ia[i * block_width];
        else
            nnz_per_tile[i] = ia[numrows] - ia[i * block_width];

        if(max_nnz < nnz_per_tile[i])
            max_nnz = nnz_per_tile[i];
        
        totalnnz += nnz_per_tile[i];
    }
    
    printf("      max_nnz in a tile =  %d\n", max_nnz);
    // printf("                 totalnnz =  %d\n", totalnnz);
    // printf("\n");

    // ------------------------------------------------------------------------------------------------------
    
    // VERSION: OMPTarget

    #pragma omp target is_device_ptr(d_blockVectorR)
    #pragma omp teams distribute parallel for //private(j)
    for (i = 0 ; i < N * blocksize ; i++)
    {
        d_blockVectorR[i] = 0.0;
    }

    double gpu_omp_csr_time = 0;
    tstart = omp_get_wtime();
    
    #if defined(NSYS)
        str = "spmm_csr_gpu";
        nvtxRangePush(str.c_str());
    #endif
    
    spmm_csr_gpu(numrows, numcols, blocksize, d_ia, d_ja, d_acsr, d_blockVectorX, d_blockVectorR);
    cudaDeviceSynchronize();
    
    #if defined(NSYS)
        nvtxRangePop();
    #endif

    tend = omp_get_wtime();
    gpu_omp_csr_time = tend - tstart;
    

    // /*
    // sanity check --> if L2 norms match accross different version
    #pragma omp target is_device_ptr(d_blockVectorR, g_norm)
    for (i = 0 ; i < N ; i++)
    {
        for (j = 0 ; j < blocksize ; j++)
        {
            g_norm[j] += (d_blockVectorR[i * blocksize + j] * d_blockVectorR[i * blocksize + j]);
        }
    }

    cuberror1 = cudaMemcpy(gpu_norm, g_norm, blocksize * sizeof(double), cudaMemcpyDeviceToHost);
    cudaDeviceSynchronize();
    
    if(cuberror1 != 0){ printf("cudaMemcpy failed gpu_norm: %d\n", cuberror);}
    
    cudaDeviceSynchronize();

    for(i = 0 ; i < blocksize ; i++)
        gpu_norm[i] = sqrt(gpu_norm[i]);

    for(i = 0 ; i < blocksize ; i++)
        cout << gpu_norm[i] << " ";
    cout << endl;
    // */

    #pragma omp target is_device_ptr(d_blockVectorR)
    #pragma omp teams distribute parallel for private(j)
    for (i = 0 ; i < N * blocksize ; i++)
    {
        d_blockVectorR[i] = 0.0;
    }

    // ------------------------------------------------------------------------------------------------------

    // VERSION: OMPTarget, blocked

    double gpu_blocked_time = 0;
    tstart = omp_get_wtime();
    for(i = 0 ; i < nrowblk ; i++)
    {
         #if defined(NSYS)
            str = "blk: " + to_string(i);;
            nvtxRangePush(str.c_str());
        #endif

        SPMM_GPU_blocked(d_ia, d_ja, d_acsr, d_blockVectorX, d_blockVectorR, 
                        numrows, numcols, blocksize, block_width, i);
        cudaDeviceSynchronize();
        
        #if defined(NSYS)
            nvtxRangePop();
        #endif
    }
    // cudaDeviceSynchronize(); // ==> how does it affect the execution
    tend = omp_get_wtime();
    gpu_blocked_time = tend - tstart;
    

    // /*
    // sanity check
    #pragma omp target is_device_ptr(d_blockVectorR, g_norm_blocked)
    for (i = 0 ; i < N ; i++)
    {
        for (j = 0 ; j < blocksize ; j++)
        {
            g_norm_blocked[j] += (d_blockVectorR[i * blocksize + j] * d_blockVectorR[i * blocksize + j]);
        }
    }
    cudaDeviceSynchronize();

    cuberror1 = cudaMemcpy(gpu_norm_blocked, g_norm_blocked, blocksize * sizeof(double), cudaMemcpyDeviceToHost);
    cudaDeviceSynchronize();
    
    if(cuberror1 != 0){ printf("cudaMemcpy failed g_norm_blocked: %d\n", cuberror);}
    
    cudaDeviceSynchronize();

    for(i = 0 ; i < blocksize ; i++)
        gpu_norm_blocked[i] = sqrt(gpu_norm_blocked[i]);

    for(i = 0 ; i < blocksize ; i++)
        cout << gpu_norm_blocked[i] << " ";
    cout << endl;
    // */
    

    // ------------------------------------------------------------------------------------------------------

    // VERSION: SpMM using cusparse library function
    // Device memory management
    
    #pragma omp target is_device_ptr(d_blockVectorR)
    #pragma omp teams distribute parallel for private(j)
    for (i = 0 ; i < N * blocksize ; i++)
    {
        d_blockVectorR[i] = 0.0;
    }

    
    double cusparse_time = 0;
    tstart = omp_get_wtime();

    #if defined(NSYS)
        str = "SpMM_cuSparse";
        nvtxRangePush(str.c_str());
    #endif

    SpMM_cuSparse(d_ia, d_ja, d_acsr, numrows, numcols, nnonzero, blocksize, d_blockVectorX, d_blockVectorR);
    
    #if defined(NSYS)
        nvtxRangePop();
    #endif
    
    tend = omp_get_wtime();
    cusparse_time = tend - tstart;

    cout << endl;
    cout << "----------------------------------------" << endl;
    cout << "CPU (OMP) time:           " << cpu_time << " sec." << endl;
    cout << "OMPTarget time:           " << gpu_omp_csr_time << " sec." << endl;
    cout << "OMPTarget, blocked time:  " << gpu_blocked_time << " sec." << endl;
    cout << "cusparseSpMM time:        " << cusparse_time << " sec." << endl;
    
    return 0;

}

void SPMM_GPU_blocked(int *row_ptr, int *col_index, double *value, double *d_Y, double *Z, 
                        int numrows, int numcols, int nvec, int block_width, int block_id)
                         
{
    // Z = A * X ==> A[numrows * numcols] Y[numrows * nvec] Z[numrows * nvec], A is the sparse matrix

    int blksz = block_width;
    int offset = block_id * block_width;

    if(offset + blksz > numrows)
        blksz = numrows - offset;
    
    int i, j, k, start, end;
    int r, c;
    double xcoef;


    // #pragma omp target is_device_ptr(row_ptr, col_index, value, d_Y, Z)\
    // firstprivate(block_id, block_width, nvec, offset)
    #pragma omp target teams distribute parallel for private(start, end, r, c, xcoef, i, j, k)\
    is_device_ptr(row_ptr, col_index, value, d_Y, Z) nowait
    //firstprivate(block_id, block_width, nvec, offset) nowait
    for(i = 0 ; i < blksz ; i++)
    {
        start = row_ptr[offset + i];
        end = row_ptr[offset + i + 1];
        for(j = start ; j < end ; j++) // for each nnz in i-th row
        {
            r = offset + i;
            c = col_index[j];
            xcoef = value[j];
            
            // #pragma omp simd
            for(k = 0 ; k < nvec ; k++) // Y[r,c] += A[r,c] * Y[c,k], for all k = 0 to nvec
            {
                Z[r * nvec + k] = Z[r * nvec + k] + xcoef * d_Y[c * nvec + k];
            }
        }
    }
} // end of SPMM_GPU_blocked

void SpMM_cuSparse(int *d_ia, int *d_ja, double *d_acsr, int numrows, int numcols, int nnonzero, int blocksize, 
                    double *d_blockVectorX, double *d_blockVectorR)
{
    // CUSPARSE APIs
    cusparseHandle_t     handle = NULL;
    cusparseSpMatDescr_t matA;
    cusparseDnMatDescr_t matB, matC;
    void*                dBuffer    = NULL;
    size_t               bufferSize = 0;
    int A_num_rows = numrows, A_num_cols = numcols, A_num_nnz = nnonzero, B_num_cols = blocksize, ldb = numcols, ldc = numrows;
    double cusparse_time = 0;
    cusparseStatus_t status;    
    double cudaAlpha = 1.0, cudaBeta = 0.0;                                           \
    
    status = cusparseCreate(&handle);
    if (status != CUSPARSE_STATUS_SUCCESS) {  cout << "Erro in cusparseCreate" << endl; } 
    
    
    // Create sparse matrix A in CSR format
    status = cusparseCreateCsr(&matA, A_num_rows, A_num_cols, A_num_nnz,
                                      d_ia, d_ja, d_acsr,
                                      CUSPARSE_INDEX_32I, CUSPARSE_INDEX_32I,
                                      CUSPARSE_INDEX_BASE_ZERO, CUDA_R_64F);

    if (status != CUSPARSE_STATUS_SUCCESS) {  cout << "Erro in matA" << endl; } 

    // Create dense matrix B
    status = cusparseCreateDnMat(&matB, A_num_cols, B_num_cols, ldb, d_blockVectorX,
                                        CUDA_R_64F, CUSPARSE_ORDER_COL);
    if (status != CUSPARSE_STATUS_SUCCESS) {  cout << "Erro in matB" << endl; } 
    // Create dense matrix C
    status = cusparseCreateDnMat(&matC, A_num_rows, B_num_cols, ldc, d_blockVectorR,
                                        CUDA_R_64F, CUSPARSE_ORDER_COL);
    if (status != CUSPARSE_STATUS_SUCCESS) {  cout << "Erro in matC" << endl; } 

    // allocate an external buffer if needed
    status = cusparseSpMM_bufferSize(
                                 handle,
                                 CUSPARSE_OPERATION_NON_TRANSPOSE,
                                 CUSPARSE_OPERATION_NON_TRANSPOSE,
                                 &cudaAlpha, matA, matB, &cudaBeta, matC, CUDA_R_64F,
                                 CUSPARSE_MM_ALG_DEFAULT, &bufferSize);
    // cout << "bufferSize: " << bufferSize << endl;
    if (status != CUSPARSE_STATUS_SUCCESS) {  cout << "Erro in cusparseSpMM_bufferSize" << endl; } 

    cudaError_t cuda_status = cudaMalloc(&dBuffer, bufferSize);
    if (cuda_status != cudaSuccess) {  cout << "Erro in dBuffer" << endl; } 

    // // execute SpMM
    status = cusparseSpMM(handle,
                                 CUSPARSE_OPERATION_NON_TRANSPOSE,
                                 CUSPARSE_OPERATION_NON_TRANSPOSE,
                                 &cudaAlpha, matA, matB, &cudaBeta, matC, CUDA_R_64F,
                                 CUSPARSE_MM_ALG_DEFAULT, dBuffer);
    if (status != CUSPARSE_STATUS_SUCCESS) {  cout << "Erro in cusparseSpMM" << endl; } 

    // destroy matrix/vector descriptors
    status = cusparseDestroySpMat(matA);
    if (status != CUSPARSE_STATUS_SUCCESS) {  cout << "Erro in cusparseDestroySpMat" << endl; }
    status = cusparseDestroyDnMat(matB);
    if (status != CUSPARSE_STATUS_SUCCESS) {  cout << "Erro in cusparseDestroySpMat" << endl; }
    status = cusparseDestroyDnMat(matC);
    if (status != CUSPARSE_STATUS_SUCCESS) {  cout << "Erro in cusparseDestroySpMat" << endl; }
    status = cusparseDestroy(handle);
    if (status != CUSPARSE_STATUS_SUCCESS) {  cout << "Erro in cusparseDestroy" << endl; }
}



void matrix_l2_norm_col_wise(double *arr, double *result, int row, int col)
{
    int i, j;
    
    // #pragma omp parallel for private(j) reduction(+:result[:col])
    for (i = 0 ; i < row ; i++)
    {
        for (j = 0 ; j < col ; j++)
        {
            result[j] += arr[i * col + j] * arr[i * col + j];
        }
    }

    for(i = 0 ; i < col ; i++)
        result[i] = sqrt(result[i]);
}

void read_csr(char *filename)
{
    int i, j;
    FILE *file = fopen(filename, "rb");
    if (filename != NULL)
    {
        fread(&numrows, sizeof(int), 1, file);
        cout << "row: " << numrows << endl;
        fread(&numcols, sizeof(int), 1, file);
        cout << "colum: " << numcols << endl;

        fread(&nnonzero, sizeof(float), 1, file);
        cout << "non zero: " << nnonzero << endl;

        ia = (int *) malloc((numrows + 1) * sizeof(int)); //colsptr
        ja = (int *) malloc(nnonzero * sizeof(int)); //irem
        acsr = (double *) malloc(nnonzero * sizeof(double)); //xrem

        fread(ia, sizeof(int), numrows + 1, file);
        cout << "finished reading ia"<<endl;

        fread(ja, sizeof(int), nnonzero, file);
        cout << "finished reading ja"<<endl;
        
        fread(acsr, sizeof(double), nnonzero, file);
        cout << "finished reading acsr"<<endl;
        fclose(file);
    }
    else
    {
        cout << "FIle opening error" << endl;
    }
}

void spmm_csr(int row, int col, int nvec, int *row_ptr, int *col_index, double *value, double *Y, double *result)
{
    int i, j, k, start, end;
    int r, c, xcoef;

    #pragma omp parallel for default(shared) private(start, end, r, c, xcoef, i, j, k)
    for(i = 0 ; i < row ; i++)
    {
        start = row_ptr[i];
        end = row_ptr[i + 1];
        
        for(j = start ; j < end ; j++)
        {
            r = i;
            c = col_index[j];
            xcoef = value[j];  
            
            // #pragma omp simd
            for(k = 0 ; k < nvec ; k++)
            {
                result[r * nvec + k] = result[r * nvec + k] + xcoef * Y[c * nvec + k];
            }
        }
    }
}

void spmm_csr_gpu(int numrows, int numcols, int nvec, int *row_ptr, int *col_index, double *value, double *d_Y, double *Z)
                         
{
    // Z = A * d_Y ==> A[numrows * numcols] Y[numrows * nvec] Z[numrows * nvec], A is the sparse matrix
    
    int i, j, k, start, end;
    int r, c;
    double xcoef;

    // #pragma omp target is_device_ptr(d_Y, row_ptr, col_index, Z, value)
    #pragma omp target teams distribute parallel for is_device_ptr(d_Y, row_ptr, col_index, Z, value)\
    private(start, end, r, c, xcoef, i, j, k) //firstprivate(nvec, ) // check without firstprivate
    for(i = 0 ; i < numrows ; i++)
    {
        start = row_ptr[i];
        end = row_ptr[i + 1];

        for(j = start ; j < end ; j++)
        {
            r = i;
            c = col_index[j];
            xcoef = value[j]; 
            #pragma omp simd 
            for(k = 0 ; k < nvec ; k++)
            {
                Z[r * nvec + k] = Z[r * nvec + k] + xcoef * d_Y[c * nvec + k];
            }
        }
    }


    // #pragma omp target teams is_device_ptr(d_Y, row_ptr, col_index, Z, value) private(r, c, xcoef, i, j, k)
    // // #pragma omp teams distribute parallel for private(start, end, r, c, xcoef, i, j, k) firstprivate(nvec, row_ptr, col_index, value, d_Y, Z) // check without firstprivate
    // for(i = 0 ; i < numrows ; i++)
    // {
    //     start = row_ptr[i];
    //     end = row_ptr[i + 1];
    //     #pragma omp distribute parallel for firstprivate(start, end)
    //     for(j = start ; j < end ; j++)
    //     {
    //         r = i;
    //         c = col_index[j];
    //         xcoef = value[j]; 
    //         #pragma omp simd 
    //         for(k = 0 ; k < nvec ; k++)
    //         {
    //             Z[r * nvec + k] = Z[r * nvec + k] + xcoef * d_Y[c * nvec + k];
    //         }
    //     }
    // }
}


void spmm_coo_gpu(int *rowind, int *colind, double *value, double *d_Y, double *Z, 
                        int numrows, int numcols, int nvec, int nnz)
                         
{
    // Z = A * d_Y ==> A[numrows * numcols] Y[numrows * nvec] Z[numrows * nvec], A is the sparse matrix
    int i, j, k, start, end;
    int r, c;
    double xcoef;

    #pragma omp target is_device_ptr(rowind, colind, d_Y, Z)
    #pragma omp teams distribute parallel for private(i, j) firstprivate(numrows, numcols, nnz, nvec, rowind, colind, value, d_Y, Z)
    for(i =  0; i < nnz ; i++)
    {
        #pragma omp simd
        for(j = 0 ; j < nvec ; j++)
        {
            // #pragma omp atomic
            Z[rowind[i] * nvec + j] = Z[rowind[i] * nvec + j] + value[i] * d_Y[colind[i] * nvec + j];
        }
    }
}

/* Haven't used the following kernels in the experiments.*/

void SpMM_cuSparse_blocked(int *d_ia, int *d_ja, double *d_acsr, int numrows, int numcols, int nnonzero, int blocksize, 
                    double *d_blockVectorX, double *d_blockVectorR, int block_id, int block_width, int *nnz_per_tile)
{
    int blksz = block_width;
    int offset = block_id * block_width;

    if(offset + blksz > numrows)
        blksz = numrows - offset;
        
    // CUSPARSE APIs
    cusparseHandle_t     handle = NULL;
    cusparseSpMatDescr_t matA;
    cusparseDnMatDescr_t matB, matC;
    void*                dBuffer    = NULL;
    size_t               bufferSize = 0;
    int A_num_rows = numrows, A_num_cols = numcols, A_num_nnz = nnonzero, B_num_cols = blocksize, ldb = numcols, ldc = blksz;
    double cusparse_time = 0;
    cusparseStatus_t status;    
    double cudaAlpha = 1.0, cudaBeta = 0.0;                                           \
    
    cout << "here 1" << endl;
    
    
    int i, j, k, start, end;
    int r, c;
    double xcoef;

    status = cusparseCreate(&handle);
    if (status != CUSPARSE_STATUS_SUCCESS) {  cout << "Erro in cusparseCreate" << endl; } 
    
    #pragma omp target is_device_ptr(d_ia)
    #pragma omp teams distribute parallel for
    for(j = offset + blksz; j >= offset ; j--)
            d_ia[j] = d_ia[j] - d_ia[offset];

    // Create sparse matrix A in CSR format
    status = cusparseCreateCsr(&matA, blksz, A_num_cols, nnz_per_tile[block_id],
                                      d_ia + offset, d_ja + d_ia[offset], d_acsr + d_ia[offset],
                                      CUSPARSE_INDEX_32I, CUSPARSE_INDEX_32I,
                                      CUSPARSE_INDEX_BASE_ZERO, CUDA_R_64F);
    cout << "here 5" << endl;

    if (status != CUSPARSE_STATUS_SUCCESS) {  cout << "Erro in matA" << endl; } 

    // Create dense matrix B
    status = cusparseCreateDnMat(&matB, A_num_cols, B_num_cols, ldb, d_blockVectorX,
                                        CUDA_R_64F, CUSPARSE_ORDER_COL);
    if (status != CUSPARSE_STATUS_SUCCESS) {  cout << "Erro in matB" << endl; } 
    // Create dense matrix C
    status = cusparseCreateDnMat(&matC, blksz, B_num_cols, ldc, d_blockVectorR + offset * B_num_cols,
                                        CUDA_R_64F, CUSPARSE_ORDER_COL);
    cout << "here 6" << endl;
    if (status != CUSPARSE_STATUS_SUCCESS) {  cout << "Erro in matC" << endl; } 

    // allocate an external buffer if needed
    status = cusparseSpMM_bufferSize(
                                 handle,
                                 CUSPARSE_OPERATION_NON_TRANSPOSE,
                                 CUSPARSE_OPERATION_NON_TRANSPOSE,
                                 &cudaAlpha, matA, matB, &cudaBeta, matC, CUDA_R_64F,
                                 CUSPARSE_MM_ALG_DEFAULT, &bufferSize);
    cout << "here 7" << endl;

    cout << "bufferSize: " << bufferSize << endl;
    if (status != CUSPARSE_STATUS_SUCCESS) {  cout << "Erro in cusparseSpMM_bufferSize" << endl; } 

    cudaError_t cuda_status = cudaMalloc(&dBuffer, bufferSize);
    if (cuda_status != cudaSuccess) {  cout << "Erro in dBuffer" << endl; } 

    cout << "here 2" << endl;
    // // execute SpMM
    status = cusparseSpMM(handle,
                                 CUSPARSE_OPERATION_NON_TRANSPOSE,
                                 CUSPARSE_OPERATION_NON_TRANSPOSE,
                                 &cudaAlpha, matA, matB, &cudaBeta, matC, CUDA_R_64F,
                                 CUSPARSE_MM_ALG_DEFAULT, dBuffer);
    if (status != CUSPARSE_STATUS_SUCCESS) {  cout << "Erro in cusparseSpMM" << endl; } 

    cout << "here 3" << endl;

    // destroy matrix/vector descriptors
    status = cusparseDestroySpMat(matA);
    if (status != CUSPARSE_STATUS_SUCCESS) {  cout << "Erro in cusparseDestroySpMat" << endl; }
    status = cusparseDestroyDnMat(matB);
    if (status != CUSPARSE_STATUS_SUCCESS) {  cout << "Erro in cusparseDestroySpMat" << endl; }
    status = cusparseDestroyDnMat(matC);
    if (status != CUSPARSE_STATUS_SUCCESS) {  cout << "Erro in cusparseDestroySpMat" << endl; }
    status = cusparseDestroy(handle);
    if (status != CUSPARSE_STATUS_SUCCESS) {  cout << "Erro in cusparseDestroy" << endl; }
}

void transpose_GPU(double *src, double *dst, const int N, const int M)
{
    //src - M * N
    int i, j;
    #pragma omp target is_device_ptr(src, dst)
    #pragma omp teams distribute parallel for collapse(2)
    for(i = 0 ; i < M ; i++)
    {
        for(j = 0 ; j < N ; j++)
        {
            dst[j * M + i] = src[i * N + j];
        }
    }
}

void read_coo(char *filename)
{
    char *coo_file = filename; 

    FILE *file = fopen(coo_file, "rb");
    if (file != NULL)
    {
        fread(&numrows_coo, sizeof(int), 1, file);
        cout << "numrows_coo: " << numrows_coo << endl;
        fread(&numcols_coo, sizeof(int), 1, file);
        cout << "numcols_coo: " << numcols_coo << endl;

        fread(&nnz_coo, sizeof(int), 1, file);
        cout << "nnz_coo: " << nnz_coo << endl;

        rowind = (int *) malloc(nnz_coo * sizeof(int)); //colsptr
        colind = (int *) malloc(nnz_coo * sizeof(int)); //irem
        value_coo = (double *) malloc(nnz_coo * sizeof(double)); //xrem

        fread(rowind, sizeof(int), nnz_coo, file);
        cout << "finished reading rowind"<<endl;

        fread(colind, sizeof(int), nnz_coo, file);
        cout << "finished reading colind"<<endl;
        
        fread(value_coo, sizeof(double), nnz_coo, file);
        cout << "finished reading value_coo"<<endl;
        fclose(file);
    }
    else
    {
        cout << "FIle opening error" << endl;
    }
}

