#!/bin/bash -l
module purge
module load esslurm
module load cdt/19.11
module load PrgEnv-cray
module switch cce cce/9.1.0-classic
# module switch cce cce/9.1.3-classic
module load craype-x86-skylake
module unload cray-libsci
module load cudatoolkit craype-accel-nvidia70
module load cuda
#module load nsight-systems
#module load cuda/10.1.168

module list
set -x

make -f Makefile_cray_classic clean
make -f Makefile_cray_classic spmm

export OMP_NUM_THREADS=5
export OMP_PLACES=cores 
export OMP_PROC_BIND=close

srun -n 1 -c 10 --cpu_bind=cores ./SpMM_mini_app.x 8 262144 ../Internship_NERSC/Matrices/twitter7_upper_40_percent_CSR.dat

#srun -n 1 -c 40 nsys profile -t cuda,osrt,nvtx,cublas -f true -o SpMM_full_csr_cray_no_cudaDeviceSynchronize.qdstrm ./SpMM_mini_app.x 8 262144 ../../Matrices/twitter7_upper_40_percent_CSR.dat
# srun -n 1 -c 40 --cpu_bind=cores ./SpMM_mini_app.x 16 262144 ../../Matrices/twitter7_top_right_CSR.dat
# srun -n 1 -c 10 --cpu_bind=cores ./SpMM_mini_app.x 12 262144 ../../Matrices/twitter7_top_left_CSR.dat
