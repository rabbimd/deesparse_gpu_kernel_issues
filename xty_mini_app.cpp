
#include <iostream>
using namespace std;

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>

// #ifdef USE_CBLAS
// #include "cblas.h"
// #elif USE_NVBLAS
// #include "nvblas.h"
// #elif USE_MKL
// #include "mkl.h"
// #include "cblas.h"
// #elif USE_CUBLAS
#include <cuda_runtime.h>
#include "cublas_v2.h"
// #elif USE_UM
// #include <cuda_runtime.h>
// #include "cublas_v2.h"
// #elif USE_CUBLAS_TILE
// #include <cuda_runtime.h>
// #include "cublas_v2.h"
// #endif

#define DGEMM_RESTRICT __restrict__

bool check_sanity(double * hostC, int N, int P, double val);

double get_seconds() {
	struct timeval now;
	gettimeofday(&now, NULL);

	const double seconds = (double) now.tv_sec;
	const double usec    = (double) now.tv_usec;

	return seconds + (usec * 1.0e-6);
}


void XTY_GPU_RED_MM(double *buf, double *result, int N, int P, int block_width)
{
    /*
    _XTY_v1_RED: adding partial sums block by block, not row by row
    Input: buf: nthds * [N * P]
    Output: result[N * P]
    nthrds : global variable, total # of threads
    buf : how to free/deallocate corresponding memory location
    */
    
    int i, j, k, l, blksz, tid, nthreads, length;
    double sum, tend;
    
    int nbuf = 128;

    int status, n = N * P;
    unsigned long int result_offset; 


    for(i = 0 ; i < N ; i = i + block_width)
    {
        blksz = block_width;
        if(i + blksz > N)
            blksz = N - i;

        #pragma omp target is_device_ptr(buf) private(sum, k, l, j)\
        firstprivate(i, n, nbuf, blksz, block_width, result_offset)\
        depend(in: N, P) depend(out: result[i * P : blksz * P]) depend(in: N, P)\
        depend(in : buf[0 * n : n], buf[1 * n : n], buf[2 * n : n], buf[3 * n : n], buf[4 * n : n], buf[5 * n : n], buf[6 * n : n], buf[7 * n : n], buf[8 * n : n], buf[9 * n : n], buf[10 * n : n],\
        buf[11 * n : n], buf[12 * n : n], buf[13 * n : n], buf[14 * n : n], buf[15 * n : n], buf[16 * n : n], buf[17 * n : n], buf[18 * n : n], buf[19 * n : n], buf[20 * n : n],\
        buf[21 * n : n], buf[22 * n : n], buf[23 * n : n], buf[24 * n : n], buf[25 * n : n], buf[26 * n : n], buf[27 * n : n], buf[28 * n : n], buf[29 * n : n], buf[30 * n : n],\
        buf[31 * n : n], buf[32 * n : n], buf[33 * n : n], buf[34 * n : n], buf[35 * n : n], buf[36 * n : n], buf[37 * n : n], buf[38 * n : n], buf[39 * n : n], buf[40 * n : n],\
        buf[41 * n : n], buf[42 * n : n], buf[43 * n : n], buf[44 * n : n], buf[45 * n : n], buf[46 * n : n], buf[47 * n : n], buf[48 * n : n], buf[49 * n : n], buf[50 * n : n],\
        buf[51 * n : n], buf[52 * n : n], buf[53 * n : n], buf[54 * n : n], buf[55 * n : n], buf[56 * n : n], buf[57 * n : n], buf[58 * n : n], buf[59 * n : n], buf[60 * n : n],\
        buf[61 * n : n], buf[62 * n : n], buf[63 * n : n], buf[64 * n : n], buf[65 * n : n], buf[66 * n : n], buf[67 * n : n], buf[68 * n : n], buf[69 * n : n], buf[70 * n : n],\
        buf[71 * n : n], buf[72 * n : n], buf[73 * n : n], buf[74 * n : n], buf[75 * n : n], buf[76 * n : n], buf[77 * n : n], buf[78 * n : n], buf[79 * n : n], buf[80 * n : n],\
        buf[81 * n : n], buf[82 * n : n], buf[83 * n : n], buf[84 * n : n], buf[85 * n : n], buf[86 * n : n], buf[87 * n : n], buf[88 * n : n], buf[89 * n : n], buf[90 * n : n],\
        buf[91 * n : n], buf[92 * n : n], buf[93 * n : n], buf[94 * n : n], buf[95 * n : n], buf[96 * n : n], buf[97 * n : n], buf[98 * n : n], buf[99 * n : n], buf[100 * n : n],\
        buf[101 * n : n], buf[102 * n : n], buf[103 * n : n], buf[104 * n : n], buf[105 * n : n], buf[106 * n : n], buf[107 * n : n], buf[108 * n : n], buf[109 * n : n], buf[110 * n : n],\
        buf[111 * n : n], buf[112 * n : n], buf[113 * n : n], buf[114 * n : n], buf[115 * n : n], buf[116 * n : n], buf[117 * n : n], buf[118 * n : n], buf[119 * n : n], buf[120 * n : n],\
        buf[121 * n : n], buf[122 * n : n], buf[123 * n : n], buf[124 * n : n], buf[125 * n : n], buf[126 * n : n], buf[127 * n : n])
        //{
        #pragma omp teams distribute parallel for    
        for(l = i ; l < (i + blksz) ; l++) //for each row in the block
        {
            for(k  = 0 ; k < P ; k++) //each col
            {
                sum = 0.0;
                #pragma omp simd
                for(j = 0 ; j < nbuf ; j++) //for each thread access corresponding N*P matrix
                {
                    sum += buf[j * N * P + l * P + k];
                }
                buf[l * P + k] = sum;
            }    
        }
        //}// end of task 
    }//end outer for
}

void print_matrix(double *mtx, int row, int col)
{
	for(int i = 0 ; i < row ; i++)
	{
		for(int j = 0 ; j < col ; j++)
		{
			cout << mtx[i * col + j] << " ";
		}
		cout << endl;
	}
}

int main(int argc, char* argv[]) 
{
	int M = atoi(argv[1]);
	int repeats = 1;
	double alpha = 1.0;
	double beta  = 1.0;
	int blksz;

	int N = atoi(argv[2]);
	int P = atoi(argv[3]);
	int block_width = atoi(argv[4]);
	
	printf("                 M =  %d\n", M);
    // printf("              Alpha =  %0.3lf\n", alpha);
    // printf("              Beta  =  %0.3lf\n", beta);
	// printf("            Repeat  =  %d\n", repeats);
	// printf("         blocksize  =  %d\n", blocksize);
	printf("                 N  =  %d\n", N);
	printf("                 P  =  %d\n", P);
	printf("         Tile size  =  %d\n", block_width);
	

	printf("Allocating Matrices...\n");
	double* DGEMM_RESTRICT matrixA = (double*) malloc(sizeof(double) * M * N);
	double* DGEMM_RESTRICT matrixB = (double*) malloc(sizeof(double) * M * P);
	double* DGEMM_RESTRICT matrixC = (double*) malloc(sizeof(double) * N * P);
	double* hostC = (double*) malloc(sizeof(double) * N * P);
	double* gemmC = (double*) malloc(sizeof(double) * N * P);

	printf("Allocation complete, populating with values...\n");

	int i, j, k, r;
	double gpu_memory;
	
	// #pragma omp parallel for
	for(i = 0 ; i < M ; i++) 
	{
		for(j = 0 ; j < N ; j++) 
		{
			matrixA[i * N + j] = 0.0025;
			// matrixB[i * blocksize + j] = 0.25;
		}
	}

	// #pragma omp parallel for
	for(i = 0 ; i < M ; i++) 
	{
		for(j = 0 ; j < P ; j++) 
		{
			// matrixA[i * N + j] = 0.25;
			matrixB[i * P + j] = 0.0025;
		}
	}

	for(i = 0 ; i < N ; i++)
	{
		for(j = 0 ; j < P ; j++)
		{
			matrixC[i * P + j] = 0;
			hostC[i * P + j] = 0;
			gemmC[i * P + j] = 0;
		}
	}
	printf("Performing multiplication...\n");

	printf("\n");
	printf("===============================================================\n");

	const double start = get_seconds();
	double sum = 0;
	
// #if defined( USE_CUBLAS ) 
  	double *devPtrA, *devPtrB, *devPtrC;
  	cudaMalloc ((void**)&devPtrA, M * N * sizeof(double));
  	cudaMalloc ((void**)&devPtrB, M * P * sizeof(double));
  	cudaMalloc ((void**)&devPtrC, N * P * sizeof(double));

	cudaMemcpy(devPtrA, matrixA, M * N * sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(devPtrB, matrixB, M * P * sizeof(double), cudaMemcpyHostToDevice);
	cudaMemset(devPtrC, 0 , N * P * sizeof(double));

  	cublasStatus_t cubstat;
  	cublasHandle_t handle;
	cudaError_t cuberror;
  	cubstat = cublasCreate(&handle);
  	if( cubstat != CUBLAS_STATUS_SUCCESS ){ printf("HandleCreationFailure\n"); return 0; }	
// #endif


	// #pragma omp target is_device_ptr(result_buf, d_memory, X, Y) firstprivate(rowOffset, buf_id, block_id, block_width)\
    // depend(in: M, N, P) depend(in: X[rowOffset * N : blksz * N], Y[rowOffset * P : blksz * P])\
    // depend(inout: result_buf[buf_id * N * P : N * P])
        // #pragma omp parallel for firstprivate(rowOffset, blksz, block_id, block_width, buf_id) private(i, j, k, total) shared(X, Y, result_buf) collapse(2)
    
	double total;
	double tstart, tend;

	tstart = get_seconds();

	#pragma omp parallel for private(i, j, k, total) collapse(2)
    for(i = 0 ; i < N ; i++)   
    {
        for(j = 0 ; j < P ; j++) 
        {   
            total = 0.0;
            // #pragma omp simd
            for (k = 0 ; k < M ; k++) // # of rows in X or Y (should be same)
            {
                total += matrixA[k * N + i] * matrixB[k * P + j];

				// cout << "total: " << total << " " << matrixA[k * N + i] * matrixB[k * P + j] << endl;;

            }
			// cout << "total: " << total << endl;
            matrixC[i * P + j] += total;
        }
		// cout << matrixC[i * P + 0] << " " << matrixA[i * N + 0] << " " << matrixB[i * P + 0] << endl;
    }
	
	tend = get_seconds();
	double cpu_time = tend - tstart;
	cout << "CPU (OMP) : " << cpu_time << " sec." << endl;
	check_sanity(matrixC, N, P, M * 0.0025 * 0.0025);


	
	tstart = get_seconds();

	#pragma omp target teams distribute parallel for is_device_ptr(devPtrA, devPtrB, devPtrC) private(i, j, k, total) collapse(2)
    for(i = 0 ; i < N ; i++)   
    {
        for(j = 0 ; j < P ; j++) 
        {   
            total = 0.0;
            // #pragma omp simd
            for (k = 0 ; k < M ; k++) // # of rows in X or Y (should be same)
            {
                total += devPtrA[k * N + i] * devPtrB[k * P + j];

				// cout << "total: " << total << " " << matrixA[k * N + i] * matrixB[k * P + j] << endl;;

            }
			// cout << "total: " << total << endl;
            devPtrC[i * P + j] += total;
        }
		// cout << matrixC[i * P + 0] << " " << matrixA[i * N + 0] << " " << matrixB[i * P + 0] << endl;
    }
	cudaDeviceSynchronize();

	tend = get_seconds();
	double omptarget_time = tend - tstart; 
	cout << "OMPTarget : " << omptarget_time << " sec." << endl;
	


	cudaMemcpy(hostC, devPtrC, N * P * sizeof(double), cudaMemcpyDeviceToHost);
	check_sanity(hostC, N, P, M * 0.0025 * 0.0025);

	cudaMemset(devPtrC, 0 , N * P * sizeof(double));

	tstart = get_seconds();
	
	cubstat = cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, P, N, M, 
							&alpha, devPtrB, P, devPtrA, N, &beta, devPtrC, N); 
	if(cubstat != CUBLAS_STATUS_SUCCESS)
		printf("cublasDgemm status: %d\n",cubstat);
	cudaDeviceSynchronize();
	
	tend = get_seconds();
	double cublas_time = tend - tstart;
	cout << "cublasDgemm : " << cublas_time << " sec." << endl;


	cudaMemcpy(gemmC, devPtrC, N * P * sizeof(double), cudaMemcpyDeviceToHost);

	check_sanity(gemmC, N, P, M * 0.0025 * 0.0025);



	// blocked version

	int nrowblk = ceil(1.0 * M / block_width);
	printf("            nrowblk =  %d\n", nrowblk);
	

	int nthreads = 128;
	double *devBuffer;
	cudaMalloc((void**) &devBuffer, nthreads * N * P * sizeof(double));
	cudaMemset(devBuffer, 0 , nthreads * N * P * sizeof(double));
	
	tstart = get_seconds();

	for(int bb = 0 ; bb < nrowblk ; bb++)
	{

	
	int block_id = bb;
	int buf_id = bb % nthreads;

	int blksz = block_width;
	if(block_id * block_width + blksz > M)
		blksz = M - block_id * block_width;
	
	int A_offset = block_id * block_width * N; 
	int B_offset = block_id * block_width * P;
	int buf_offset = buf_id * N * P;

	// #pragma omp target is_device_ptr(result_buf, d_memory, X, Y) firstprivate(rowOffset, buf_id, block_id, block_width)\
    // depend(in: M, N, P) depend(in: X[rowOffset * N : blksz * N], Y[rowOffset * P : blksz * P])\
    // depend(inout: result_buf[buf_id * N * P : N * P])
        // #pragma omp parallel for firstprivate(rowOffset, blksz, block_id, block_width, buf_id) private(i, j, k, total) shared(X, Y, result_buf) collapse(2)

	// #pragma omp target teams distribute parallel for is_device_ptr(devPtrA, devPtrB, devPtrC) private(i, j, k, total) collapse(2) //firstprivate(rowOffset, buf_id, block_id, block_width)\
    depend(in: M, N, P) depend(in: X[rowOffset * N : blksz * N], Y[rowOffset * P : blksz * P])\
    depend(inout: result_buf[buf_id * N * P : N * P])
    // #pragma omp teams distribute parallel for firstpriBvate(rowOffset, blksz, block_id, block_width, buf_id) private(i, j, k, total) shared(X, Y, result_buf) collapse(2)

	// #pragma omp target is_device_ptr(devPtrA, devPtrB, devPtrC) private(i, j, k, total) 
	// firstprivate(A_offset, B_offset, block_id, block_width, buf_id) depend(out: devBuffer[buf_id * N * P : N * P])
    // #pragma omp teams distribute parallel for  collapse(2)

	#pragma omp target is_device_ptr(devBuffer, devPtrA, devPtrB) firstprivate(A_offset, B_offset, buf_offset, buf_id, block_id, block_width)\
    depend(in: M, N, P) depend(inout: devBuffer[buf_id * N * P : N * P]) //nowait
    #pragma omp teams distribute parallel for firstprivate(A_offset, B_offset, buf_offset, buf_id, block_id, block_width) private(i, j, k, total) shared(devBuffer, devPtrA, devPtrB) collapse(2)
    for(i = 0 ; i < N ; i++)   
    {
        for(j = 0 ; j < P ; j++) 
        {   
            total = 0.0;
            for (k = 0 ; k < blksz ; k++) // # of rows in X or Y (should be same)
            {
                total += devPtrA[A_offset + k * N + i] * devPtrB[B_offset + k * P + j];
            }
            devBuffer[buf_offset + i * P + j] += total;
        }
    }

	}
	// cudaDeviceSynchronize();

	XTY_GPU_RED_MM(devBuffer, devPtrC, N, P, block_width);
	cudaDeviceSynchronize();
	tend = get_seconds();
	double omptarget_blocked_time = tend - tstart;

	cout << "OMPTarget, blocked : " << omptarget_blocked_time << " sec." << endl;

	cudaMemcpy(gemmC, devPtrC, N * P * sizeof(double), cudaMemcpyDeviceToHost);
	check_sanity(gemmC, N, P, M * 0.0025 * 0.0025);
	// print_matrix(gemmC, N, P);







	// Repeat multiple times
	// for(r = 0; r < repeats; r++) 
	// {

	// #if defined( USE_MKL ) || defined (USE_CBLAS)
	// 	// I didn't finish the Intel MKL version on CPU
	// 	beta = 0.0;
	// 	cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans,
	// 			blocksize, blocksize, N, alpha, matrixA, blocksize, matrixB, blocksize, beta, matrixC, blocksize);
	
	// #elif defined( USE_NVBLAS )
	// 	char transA = 'N';
	// 	char transB = 'N';

	// 	dgemm(&transA, &transB, &N, &N, &N, &alpha, matrixA, &N,
	// 		matrixB, &N, &beta, matrixC, &N);

	// #elif defined(USE_CUBLAS)
		// This is my tiling version
	// 	int nrowblk = ceil(1.0 * N / block_width);
	// 	printf("            nrowblk =  %d\n", nrowblk);
	// 	gpu_memory = 1.0 * 1e-9 * ((2 * blocksize * block_width * sizeof(double) + blocksize * blocksize * sizeof(double)));
	// 	printf("    GPU memory used =  %0.3lf GB\n", gpu_memory);

	// 	cudaMemset(devPtrC, 0.0, blocksize * blocksize * sizeof(double));

	// 	for(i = 0 ; i < nrowblk ; i++)
	// 	{
	// 		blksz = block_width;
	// 		if(i * block_width + blksz > N)
	// 			blksz = N - i * block_width;
			
	// 		cuberror = cudaMemcpy((void *)devPtrA, matrixA + (i * block_width * blocksize), blksz * blocksize * sizeof(double), cudaMemcpyHostToDevice);
	// 		if(cuberror != 0) { printf("cudaMalloc Filed to copy devPtrA, block : %d\n", i); return 0; }

	// 		cuberror = cudaMemcpy((void *)devPtrB, matrixB + (i * block_width * blocksize), blksz * blocksize * sizeof(double), cudaMemcpyHostToDevice);
	// 		if(cuberror != 0) { printf("cudaMalloc Filed to copy devPtrB, block : %d\n", i); return 0; }
			
	// 		cudaDeviceSynchronize();

	// 		cubstat = cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, blocksize, blocksize, blksz, 
	// 				&alpha, devPtrB, blocksize, devPtrA, blocksize, &beta, devPtrC, blocksize); 
	// 		if(cubstat != CUBLAS_STATUS_SUCCESS)
	// 			printf("cublasDgemm status: %d\n",cubstat);
	// 		cudaDeviceSynchronize();
	// 	}	
	// 	cuberror = cudaMemcpy(matrixC, devPtrC, blocksize * blocksize * sizeof(double), cudaMemcpyDeviceToHost);
    // 	if( cuberror != 0 ){ printf("cudaMemcpy failed temp at blok_id: %d errocode: %d\n", i, cuberror);}
	// // #elif defined( USE_UM )
	// 	cublasStatus_t cubstat;
	// 	cublasHandle_t handle;
	// 	beta = 0.0;
	// 	cubstat = cublasCreate(&handle);
	// 	if( cubstat != CUBLAS_STATUS_SUCCESS ){ printf("HandleCreationFailure\n"); return 0; }
	// 	cubstat = cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, blocksize, blocksize, N, 
	// 				&alpha, matrixB, blocksize, matrixA, blocksize, &beta, matrixC, blocksize); 
	// 	if(cubstat != CUBLAS_STATUS_SUCCESS)
	// 		printf("cublasDgemm status: %d\n",cubstat);
	// // #else
	
		// #pragma omp parallel for private(sum)
		// #pragma acc parallel loop
		// for(i = 0; i < N; i++) {
		// 	#pragma acc loop
		// 	for(j = 0; j < N; j++) {
		// 		sum = 0;
		// 		for(k = 0; k < N; k++) {
		// 			sum += matrixA[i * N + k] * matrixB[k * N + j];
		// 		}
		// 		matrixC[i * N + j] = (alpha * sum) + (beta * matrixC[i * N + j]);
		// 	}
		// }
	// #endif
	
	// } // end repeat loop

// #if defined( USE_CUBLAS ) || defined(USE_CUBLAS_TILE)
	cudaFree (devPtrA);
	cudaFree (devPtrB);
	cudaFree (devPtrC);
	cublasDestroy(handle); 
// #elif defined( USE_UM )
  	cudaDeviceSynchronize();
// #endif
	const double end = get_seconds();

	// double matrix_memory = 1e-9 * 2 * N * blocksize * sizeof(double) + 1e-9 * blocksize * blocksize * sizeof(double);
	const double time_taken = (end - start);
	// const double flops_computed = 1e-9 * N * blocksize * blocksize * 2.0 * (double)(repeats) + 1e-9 * blocksize * blocksize * 2 * (double)(repeats);
	// printf("      Multiply time =  %0.3lf seconds\n", time_taken);
	// printf("     FLOPs computed =  %0.3lf GF\n", flops_computed);
	// printf("       GFLOP/s rate =  %0.3lf GF/s\n", flops_computed / time_taken);
	// printf("Memory for Matrices =  %0.3lf GB\n", matrix_memory);

	printf("===============================================================\n");
	printf("\n");

	// printf("Printing Result matrix: \n");
	// for(i = 0 ; i < blocksize ; i++)
	// {
	// 	for(j = 0 ; j < blocksize ; j++)
	// 		printf("%0.1lf ", matrixC[i * blocksize + j]);
	// 	printf("\n");
	// }
	
		
// #if defined(USE_CUBLAS)
	printf("%d,%d,%d,%d,%0.3lf,%0.3lf,%0.3lf,%0.3lf\n\n", M, N, P, block_width, cpu_time, cublas_time, omptarget_time, omptarget_blocked_time);
// #elif defined(USE_UM)
// 	printf("%d,%d,%0.3lf,%0.3lf,%0.3lf\n\n", N, blocksize, matrix_memory, time_taken, flops_computed / time_taken);
// #endif

	free(matrixA);
	free(matrixB);
	free(matrixC);

	return 0;
}

bool check_sanity(double * hostC, int N, int P, double val)
{
	int i, j;

	const double allowed_margin = 0.001;

 	for(i = 0 ; i < N ; i++)
	{
		for(j = 0 ; j < P ; j++)
		{
			// cout << hostC[i * N + j] << " ";
			if(abs(hostC[i * N + j] - val) > allowed_margin)
			{
				cout << "(" << i << "," << j << ") : " << hostC[i * N + j] << " " << val << " -> Solution check FAILED" << endl; 
				printf(" -> Solution check FAILED.\n");
				return false;
			}
		}
		// cout << endl;
	}
	printf(" -> Solution check PASSED successfully.\n\n");
	return true;
}
