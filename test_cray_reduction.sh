#!/bin/bash


module purge
module load cdt/19.11
module load PrgEnv-cray
module switch cce cce/9.1.0-classic
module load craype-x86-skylake
module unload cray-libsci
module load cudatoolkit craype-accel-nvidia70
module load cuda

set -x

export OMP_NUM_THREADS=20
export OMP_PLACES=cores 
export OMP_PROC_BIND=close

rm *.x
CC -h omp -O3 -h std=c++11 -h noacc -I${CUDA_ROOT}/include -L${CUDA_ROOT}/lib64 -lcudart mat_reduction_openmp.cpp -o mat_reduction_openmp.x

# srun -n 1 -c 10 --cpu_bind=cores valgrind --leak-check=full --show-leak-kinds=all ./mat_reduction_openmp.x #128568730 8 26214

srun -n 1 -c 40 --cpu_bind=cores ./mat_reduction_openmp.x