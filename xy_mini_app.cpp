#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#include "omp.h"
#include <string>
#include <iostream>
#include <cstring>
using namespace std;


#include <cuda_runtime.h>
#include "cublas_v2.h"

#if defined(NSYS)
#include <nvToolsExt.h>
#endif

#define DGEMM_RESTRICT __restrict__

double get_seconds() 
{
	struct timeval now;
	gettimeofday(&now, NULL);

	const double seconds = (double) now.tv_sec;
	const double usec    = (double) now.tv_usec;

	return seconds + (usec * 1.0e-6);
}

// CPU
void omp_mm(double *a, int row_a, int col_a, double *b, int row_b,int col_b, double *c)
{
	if ( col_a != row_b ) 
    {
		return; 
    }

	int i, j, k;
	int index;
	int border = row_a * col_b;
    i = 0;
    j = 0;
    #pragma omp parallel for private(i,j,k)
	for (index = 0; index < border; index++) 
    {
        i = index / col_b; j = index % col_b;
		int row_i = i * col_a; 
		int row_c = i * col_b;
        c[row_c + j] = 0;
		for ( k = 0; k < row_b; k++ ) 
		{
            c[row_c + j] += a[row_i + k] * b[k * col_b + j]; // C[i,j] = A[i:] * B[:j]
        }
    } 
}

void xy_gpu_nested_loop(double *matrixA, double *matrixB, double *matrixC,
                                int N, int M, int P /*, int block_width, int block_id */)
{
	int i, j, k;
	double total;

    #pragma omp target is_device_ptr(matrixA, matrixB, matrixC) 
    #pragma omp teams distribute parallel for private(i, j, k, total) //collapse(2) // --> adding collpase() slows down with CCE compiler -- weird
    for(i = 0 ; i < N ; i++)   // row  
    {
        for(j = 0 ; j < P ; j++) // col
        {
            total = 0.0;
            for (k = 0 ; k < M ; k++)
            {
                total += matrixA[i * M + k] * matrixB[k * P + j];
            }
            matrixC[i * P + j] = total;
        }
    }
}


void xy_gpu_nested_loop_blocked(double *matrixA, double *matrixB, double *matrixC,
                                int N, int M, int P, int block_width, int block_id)
{
	int i, j, k, blksz; 
	unsigned long int a_offset, c_offset;
	double total;

	blksz = block_width;
	
	a_offset = block_id * block_width * M;
	c_offset = block_id * block_width * P;

	if(block_id * block_width + blksz > N)
		blksz = N - block_id * block_width;
	
	

    #pragma omp target is_device_ptr(matrixA, matrixB, matrixC) firstprivate(a_offset, c_offset, blksz) //nowait
    #pragma omp teams distribute parallel for private(i, j, k, total) //collapse(2) // --> adding collpase() slows down with CCE compiler -- weird
    for(i = 0 ; i < blksz ; i++)    
    {
        for(j = 0 ; j < P ; j++)
        {
            total = 0.0;
            for (k = 0 ; k < M ; k++)
            {
                total += matrixA[a_offset + i * M + k] * matrixB[k * P + j];
            }
            matrixC[c_offset + i * P + j] = total;
        }
    }
}

void omp_mm_gpu(double *a, int row_a, int col_a, double *b, int row_b,int col_b, double *c)
{
	if ( col_a != row_b ) 
    {
		return; 
    }

	int i, j, k;
	int index;
	int border = row_a * col_b;
    i = 0;
    j = 0;
	#pragma omp target is_device_ptr(a, b, c)
    #pragma omp teams distribute parallel for private(i,j,k) //num_threads(dtn(border, 1))
	for (index = 0; index < border; index++) 
    {
        i = index / col_b; j = index % col_b;
		int row_i = i * col_a; 
		int row_c = i * col_b;
        c[row_c + j] = 0;
		for ( k = 0; k < row_b; k++ ) 
		{
            c[row_c + j] += a[row_i + k] * b[k * col_b + j]; 
        }
    } 
}


void omp_mm_gpu_blocked(double *matrixA, double *matrixB, double *matrixC,
                                int N, int M, int P, int block_width, int block_id)
{
	int i, j, k, blksz, index; 
	unsigned long int a_offset, c_offset;
	double total;

	blksz = block_width;
	
	a_offset = block_id * block_width * M;
	c_offset = block_id * block_width * P;

	if(block_id * block_width + blksz > N)
		blksz = N - block_id * block_width;

	
    #pragma omp target is_device_ptr(matrixA, matrixB, matrixC) firstprivate(a_offset, c_offset, blksz) nowait
    #pragma omp teams distribute parallel for private(i, j, k, total) //collapse(2) // --> adding collpase() slows down with CCE compiler -- weird
    for(index = 0 ; index < blksz * P ; index++)    
    {
            total = 0.0;
			i = index / M; j = index % M;
            for (k = 0 ; k < M ; k++)
            {
                total += matrixA[a_offset + i * M + k] * matrixB[k * P + j];
            }
            matrixC[c_offset + i * P + j] = total;
    }
}

void set_zero_host_matrix(double *matrixC, int row, int col)
{
	int i;
	#pragma omp parallel for
	for(i = 0; i < row * col; i++) 
	{
		matrixC[i] = 0.0;
	}
}

void sanity_check(double *matrixC, int row, int col, int val)
{
	int i, j;
	double allowed_margin = 1.0e-8;

	for(i = 0 ; i < row ; i++)
	{
		for(j = 0 ; j < col ; j++)
		{
			if((matrixC[i * col + j] - val) > allowed_margin)
			{
				printf("(%d, %d) : %lf %lf -> Solution check FAILED.\n", i, j, matrixC[i * col + j], val);
				return;
			}
		}
	}
	printf(" -> Solution check PASSED successfully.\n\n");
}

void copy_to_device(double *devPtr, double *hostPtr, int row, int col)
{
	cudaError_t cuberror;
	cuberror = cudaMemcpy((void *)devPtr, hostPtr, row * col * sizeof(double), cudaMemcpyHostToDevice);
  	cudaDeviceSynchronize();
	if( cuberror != cudaSuccess ){ printf("H2D: cudaMemcpy Failure on devPtrC\n"); return; }
}

void copy_to_host(double *hostPtr, double *devPtr, int row, int col)
{
	cudaError_t cuberror;
	cuberror = cudaMemcpy(hostPtr, devPtr, row * col * sizeof(double), cudaMemcpyDeviceToHost);
  	cudaDeviceSynchronize();
	if( cuberror != cudaSuccess ){ printf("D2H: cudaMemcpy Failure on devPtrC\n"); return; }
}

int main(int argc, char* argv[]) 
{
	int N = 30, M, P;
	int repeats = 1;
	string str;

	double alpha = 1.0;
	double beta  = 0.0;
	double gpu_memory, tstart, tend;
	double cpu_time,omptarget_time,omptarget_nested_time,cuda_time,cuda_blocked_time,omptarget_nested_blocked_time,omp_mm_gpu_blocked_time;
	int blksz;

	N = atoi(argv[1]);
	M = atoi(argv[2]);
	P = atoi(argv[3]);
		

	int block_width = atoi(argv[4]);
  	
	double matrix_memory = 1e-9 * N * M * sizeof(double) + 1e-9 * M * P * sizeof(double) + 1e-9 * N * P * sizeof(double);
	if(matrix_memory > 15.5)
	{
		printf("Problem size is bigger than GPU memory: %lf\n", matrix_memory);
		return 0;
	}

	printf("                  N =  %d\n", N);
	printf("                  M =  %d\n", M);
	printf("                  P =  %d\n", P);
    printf("              Alpha =  %0.3lf\n", alpha);
    printf("              Beta  =  %0.3lf\n", beta);
	printf("            Repeat  =  %d\n", repeats);
// #if defined(USE_CUBLAS)
	printf("         Tile size  =  %d\n", block_width);
// #endif
	double* DGEMM_RESTRICT matrixA = (double*) malloc(sizeof(double) * N * M);
	double* DGEMM_RESTRICT matrixB = (double*) malloc(sizeof(double) * M * P);
	double* DGEMM_RESTRICT matrixC = (double*) malloc(sizeof(double) * N * P);


	double* DGEMM_RESTRICT temp = (double*) malloc(sizeof(double) * block_width * M);

	printf("Allocation complete, populating with values...\n");

	int i, j, k, r;

	#pragma omp parallel for
	for(i = 0; i < N; i++) {
		for(j = 0; j < M; j++) {
			matrixA[i * M + j] = 2.0;
		}
	}
	
	#pragma omp parallel for
	for(i = 0; i < M; i++) {
		for(j = 0; j < P; j++) {
			matrixB[i * P + j] = 0.5;
		}
	}
	#pragma omp parallel for
	for(i = 0; i < N; i++) {
		for(j = 0; j < P; j++) {
			matrixC[i * P + j] = 0.0;
		}
	}

	const double start = get_seconds();
	double *devPtrA, *devPtrB, *devPtrC;
	cudaMalloc ((void**)&devPtrA, N * M * sizeof(double));
	cudaMalloc ((void**)&devPtrB, M * P * sizeof(double));
	cudaMalloc ((void**)&devPtrC, N * P * sizeof(double));

	cublasStatus_t cubstat;
	cublasHandle_t handle;
	cudaError_t cuberror;
	
	cubstat = cublasCreate(&handle);
	if( cubstat != CUBLAS_STATUS_SUCCESS ){ printf("HandleCreationFailure - 1 \n"); return 0; }
	
	copy_to_device(devPtrA, matrixA, N, M);
	copy_to_device(devPtrB, matrixB, M, P);
	copy_to_device(devPtrC, matrixC, N, P);

	// Repeat multiple times
	for(r = 0 ; r < repeats; r++) 
	{
		printf("===============================================================\n");
		printf("\n");

		tstart = get_seconds();
		omp_mm(matrixA, N, M, matrixB, M, P, matrixC);
		tend = get_seconds();
		cpu_time = tend - tstart;
		printf("CPU (OpenMP) time: %.3lf\n", tend - tstart);
		
		sanity_check(matrixC, N, P, M * 2 * 0.50);
		set_zero_host_matrix(matrixC, N, P);
		copy_to_device(devPtrC, matrixC, N, P);

		printf("===============================================================\n");
		printf("\n");

		// GPU: Full XY
		// GFLOPS ==> measures this one
		tstart = get_seconds();
		#if defined(NSYS)
        	str = "omp_mm_gpu";
        	nvtxRangePush(str.c_str());
    	#endif
		
		omp_mm_gpu(devPtrA, N, M, devPtrB, M, P, devPtrC);
		cudaDeviceSynchronize();
		
		#if defined(NSYS)
        	nvtxRangePop();
    	#endif

		tend = get_seconds();	
		omptarget_time = tend - tstart;
		printf("GPU (OpenMP) time: %.3lf\n", tend - tstart);

		copy_to_host(matrixC, devPtrC, N, P);
		sanity_check(matrixC, N, P, M * 2 * 0.50);
		set_zero_host_matrix(matrixC, N, P);
		copy_to_device(devPtrC, matrixC, N, P);

		printf("===============================================================\n");
		printf("\n");

		tstart = get_seconds();
		#if defined(NSYS)
        	str = "xy_gpu_nested_loop";
        	nvtxRangePush(str.c_str());
    	#endif

		xy_gpu_nested_loop(devPtrA, devPtrB, devPtrC, N, M, P);
		cudaDeviceSynchronize();
		
		#if defined(NSYS)
        	nvtxRangePop();
    	#endif

		tend = get_seconds();
		omptarget_nested_time = tend - tstart;
		printf("GPU (OpenMP, nested loop) time: %.3lf\n", tend - tstart);

		copy_to_host(matrixC, devPtrC, N, P);
		sanity_check(matrixC, N, P, M * 2 * 0.50);
		set_zero_host_matrix(matrixC, N, P);
		copy_to_device(devPtrC, matrixC, N, P);
		
		printf("===============================================================\n");
		printf("\n");

		// GPU: cublasDgemm FUll XY

		tstart = get_seconds();

		#if defined(NSYS)
        	str = "cublasDgemm";
        	nvtxRangePush(str.c_str());
    	#endif

		cubstat = cublasDgemm( handle, CUBLAS_OP_N, CUBLAS_OP_N, P, N, M,
					&alpha, devPtrB, P, devPtrA, M, &beta, devPtrC, P);
		cudaDeviceSynchronize();
		
		#if defined(NSYS)
        	nvtxRangePop();
    	#endif

		if(cubstat != CUBLAS_STATUS_SUCCESS){ printf("cublasDgemm Failed in Tiling\n"); return 0; }
		tend = get_seconds();
		cuda_time = tend - tstart;
		printf("Full cublasDgemm time: %.3lf\n", tend - tstart);

		copy_to_host(matrixC, devPtrC, N, P);
		sanity_check(matrixC, N, P, M * 2 * 0.50);
		set_zero_host_matrix(matrixC, N, P);
		copy_to_device(devPtrC, matrixC, N, P);

		printf("===============================================================\n");
		printf("\n");

		// GPU: cublasDgemm blocked XY

		int nrowblk = ceil(1.0 * N/block_width);
		printf("            nrowblk =  %d\n", nrowblk);
		gpu_memory = 1e-9 * N * M * sizeof(double) + 1e-9 * M * P * sizeof(double) + 1e-9 * N * P * sizeof(double);
		printf("    GPU memory used =  %0.3lf GB\n", gpu_memory);
		
		tstart = get_seconds();

		#if defined(NSYS)
        	str = "cublasDgemm_blocked";
        	nvtxRangePush(str.c_str());
    	#endif

		for(i = 0 ; i < nrowblk ; i++)
		{
			blksz = block_width;
			if(i * block_width + blksz > N)
				blksz = N - i * block_width;
			
			#if defined(NSYS)
        		str = "cublas: " + to_string(i);
        		nvtxRangePush(str.c_str());
    		#endif

			cubstat = cublasDgemm( handle, CUBLAS_OP_N, CUBLAS_OP_N, P, blksz, M,
					&alpha, devPtrB, P, devPtrA+(i*block_width*M), M, &beta, devPtrC+(i*block_width*P), P);
			cudaDeviceSynchronize();

			#if defined(NSYS)
        		nvtxRangePop();
    		#endif

			if(cubstat != CUBLAS_STATUS_SUCCESS){ printf("cublasDgemm Failed in Tiling\n"); return 0; }
		}

		#if defined(NSYS)
        	nvtxRangePop();
    	#endif

		tend = get_seconds();

		cuda_blocked_time = tend - tstart;
		const double time_taken = (tend - tstart);
		const double flops_computed = 1e-9 * 2 * N * M * P * (double)(repeats) + 1e-9 * 2 * N * P * (double)(repeats);
	
		printf("      Multiply time =  %0.3lf seconds\n", time_taken);
		printf("     FLOPs computed =  %0.3lf GF\n", flops_computed);
		printf("       GFLOP/s rate =  %0.3lf GF/s\n", flops_computed / time_taken);
		printf("Memory for Matrices =  %0.3lf GB\n", matrix_memory);

		copy_to_host(matrixC, devPtrC, N, P);
		sanity_check(matrixC, N, P, M * 2 * 0.50);
		set_zero_host_matrix(matrixC, N, P);
		copy_to_device(devPtrC, matrixC, N, P);

		printf("===============================================================\n");
		printf("\n");

		tstart = get_seconds();

		#if defined(NSYS)
        	str = "xy_gpu_nested_loop_blocked";
        	nvtxRangePush(str.c_str());
    	#endif

		for(i = 0 ; i < nrowblk ; i++)
		{
			// printf("block_id: %d\n", i); 
			#if defined(NSYS)
        		str = "omp: " + to_string(i);
        		nvtxRangePush(str.c_str());
    		#endif
			xy_gpu_nested_loop_blocked(devPtrA, devPtrB, devPtrC, N, M, P, block_width, i);
			cudaDeviceSynchronize();

			#if defined(NSYS)
        		nvtxRangePop();
    		#endif
		}

		#if defined(NSYS)
        	nvtxRangePop();
    	#endif
		cudaDeviceSynchronize();
		tend = get_seconds();
		omptarget_nested_blocked_time = tend - tstart;
		printf("GPU (OMP Target) blocked nested loop: %.3lf\n", omptarget_nested_blocked_time);
		copy_to_host(matrixC, devPtrC, N, P);
		sanity_check(matrixC, N, P, M * 2 * 0.50);
		set_zero_host_matrix(matrixC, N, P);
		copy_to_device(devPtrC, matrixC, N, P);

		printf("===============================================================\n");
		printf("\n");


		tstart = get_seconds();

		#if defined(NSYS)
        	str = "xy_gpu_nested_loop_blocked";
        	nvtxRangePush(str.c_str());
    	#endif

		for(i = 0 ; i < nrowblk ; i++)
		{
			// printf("block_id: %d\n", i); 
			#if defined(NSYS)
        		str = "omp: " + to_string(i);
        		nvtxRangePush(str.c_str());
    		#endif
			omp_mm_gpu_blocked(devPtrA, devPtrB, devPtrC, N, M, P, block_width, i);
			// omp_mm_gpu_blocked(devPtrA, N, M, devPtrB, M, P, devPtrC, block_width, i);
			// cudaDeviceSynchronize();

			#if defined(NSYS)
        		nvtxRangePop();
    		#endif
		}

		#if defined(NSYS)
        	nvtxRangePop();
    	#endif

		cudaDeviceSynchronize();

		tend = get_seconds();
		omp_mm_gpu_blocked_time = tend - tstart;
		printf("omp_mm_gpu_blocked time : %.3lf\n", omp_mm_gpu_blocked_time);
		copy_to_host(matrixC, devPtrC, N, P);
		sanity_check(matrixC, N, P, M * 2 * 0.50);
		
		// for(i = 0 ; i < 10 ; i++)
		// {
		// 	for(j = 0 ; j < P ; j++)
		// 	{
		// 		cout << matrixC[i * P + j] << " ";
		// 	}
		// 	cout << endl;
		// }

	} // end of repeat loop

	
	
	
	printf("===============================================================\n");
	printf("\n");

	printf("block_width,cpu_time,omptarget_time,omptarget_nested_time,cuda_time,cuda_blocked_time,omptarget_nested_blocked_time_nowait,omp_mm_gpu_blocked_time\n");
	printf("%d,%.3lf,%.3lf,%.3lf,%.3lf,%.3lf,%.3lf,%.3lf\n", block_width,cpu_time,omptarget_time,omptarget_nested_time,cuda_time,cuda_blocked_time,omptarget_nested_blocked_time,omp_mm_gpu_blocked_time);

	cudaFree (devPtrA);
	cudaFree (devPtrB);
	cudaFree (devPtrC);
	cublasDestroy(handle);

	free(matrixA);
	free(matrixB);
	free(matrixC);

	return 0;
}
